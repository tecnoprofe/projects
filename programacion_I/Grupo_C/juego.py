import pygame
import sys
#  inicializar Pygame
pygame.init()
# Definir colores
NEGRO = (0, 0, 0)
BLANCO = (255, 255, 255)
ROJO = (255, 0, 0)
AZUL = (0, 0, 255)
# Configuración de la pantalla
ANCHO = 1550
ALTO = 800
pantalla = pygame.display.set_mode((ANCHO, ALTO))
pygame.display.set_caption("Laberinto")
# Calcular el tamaño de celda para que el laberinto coincida con la pantalla
tamano_celda = min(ANCHO // 20, ALTO // 20)
# Definir los jugadores (cuadrados)
cuadrado = pygame.Rect(1480, 40, 40, 40)
cuadrado2 = pygame.Rect(40, 40, 40, 40)
# Después de definir los jugadores (cuadrados)
cuadrado = pygame.Rect(1480, 40, 40, 40)
cuadrado2 = pygame.Rect(40, 40, 40, 40)
# Agregar la definición del círculo verde
circulo_verde = pygame.Rect(1320, 680, 40, 40)
# Función para dibujar el círculo verde
def dibujar_circulo_verde():
    pygame.draw.circle(pantalla, (0, 255, 0), (circulo_verde.x + circulo_verde.width // 2, circulo_verde.y + circulo_verde.height // 2), circulo_verde.width // 2)
# Velocidad de movimiento
velocidad = 10
# Crear laberinto
laberinto = [
    "***************************************",
    "*     *       *     *                 *",
    "* *** * ***** * * * * * ************* *",
    "*   *   *   *   * *   *   *     *     *",
    "* * *** * * ******* ***** * *** * *** *",
    "* *   *   *       *           * * * * *",
    "* *** * * ********* *********** * * * *",
    "* *   * * *       * *         * *   * *",
    "* * *** * * ***** * * ******* * ***** *",
    "* * *   *   *   * * * *     * *     * *",
    "* * * ***** *** * * * * *** * * *** * *",
    "* * *     *     *   * * * * * *   * * *",
    "* * ***** ********* * * * * ********* *",
    "* * *   *           * * * *           *",
    "* * * *** *********** * * *************",
    "* * * * *             * *             *",
    "* * * * * *   ** **  ***  ********* * *",
    "* *   * * *****   ****        *** * * *",
    "* * *        *  *     ***   *   *     *",
    "***************************************",
]
# Función para dibujar el laberinto en la pantalla
def dibujar_laberinto():
    for fila, linea in enumerate(laberinto):
        for col, caracter in enumerate(linea):
            if caracter == "*":
                pygame.draw.rect(pantalla, BLANCO, (col * tamano_celda, fila * tamano_celda, tamano_celda, tamano_celda))
# Función para manejar eventos
def manejar_eventos():
    for evento in pygame.event.get():
        if evento.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
# Función para mover al jugador 1
def mover_jugador(teclas):
    global velocidad
    if teclas[pygame.K_LEFT] and cuadrado.x > 0 and not colision_con_pared(cuadrado, -velocidad, 0):
        cuadrado.x -= velocidad
    if teclas[pygame.K_RIGHT] and cuadrado.x < ANCHO - cuadrado.width and not colision_con_pared(cuadrado, velocidad, 0):
        cuadrado.x += velocidad
    if teclas[pygame.K_UP] and cuadrado.y > 0 and not colision_con_pared(cuadrado, 0, -velocidad):
        cuadrado.y -= velocidad
    if teclas[pygame.K_DOWN] and cuadrado.y < ALTO - cuadrado.height and not colision_con_pared(cuadrado, 0, velocidad):
        cuadrado.y += velocidad
    if colision_con_circulo(cuadrado):
        print("¡Jugador Rojo gana!")
        pygame.quit()
        sys.exit()
    # Verificar colisión con el jugador 2
    if cuadrado.colliderect(cuadrado2):
        cuadrado.x = 1480
        cuadrado.y = 40
# Función para mover al jugador 2
def mover_jugador2(teclas):
    global velocidad
    if teclas[pygame.K_a] and cuadrado2.x > 0 and not colision_con_pared(cuadrado2, -velocidad, 0):
        cuadrado2.x -= velocidad
    if teclas[pygame.K_d] and cuadrado2.x < ANCHO - cuadrado2.width and not colision_con_pared(cuadrado2, velocidad, 0):
        cuadrado2.x += velocidad
    if teclas[pygame.K_w] and cuadrado2.y > 0 and not colision_con_pared(cuadrado2, 0, -velocidad):
        cuadrado2.y -= velocidad
    if teclas[pygame.K_s] and cuadrado2.y < ALTO - cuadrado2.height and not colision_con_pared(cuadrado2, 0, velocidad):
        cuadrado2.y += velocidad
    if colision_con_circulo(cuadrado2):
        print("¡Jugador Azul gana!")
        pygame.quit()
        sys.exit()
    # Verificar colisión con el jugador 1
    if cuadrado2.colliderect(cuadrado):
        cuadrado2.x = 40
        cuadrado2.y = 40
# Función para verificar colisión con la pared en una dirección específica
def colision_con_pared(jugador, dx, dy):
    jugador_rect = pygame.Rect(jugador.x + dx, jugador.y + dy, jugador.width, jugador.height)
    for fila, linea in enumerate(laberinto):
        for col, caracter in enumerate(linea):
            if caracter == "*":
                pared_rect = pygame.Rect(col * tamano_celda, fila * tamano_celda, tamano_celda, tamano_celda)
                if jugador_rect.colliderect(pared_rect):
                    return True
    return False
# Agregar la función para verificar colisión con el círculo verde
def colision_con_circulo(jugador):
    return jugador.colliderect(circulo_verde)
# Función para actualizar la pantalla
def actualizar_pantalla():
    pantalla.fill(NEGRO)
    dibujar_laberinto()
    dibujar_circulo_verde()
    # Dibujar cuadrado rojo (jugador 1)
    pygame.draw.rect(pantalla, ROJO, cuadrado)
    # Dibujar cuadrado azul (jugador 2)
    pygame.draw.rect(pantalla, AZUL, cuadrado2)
    pygame.display.flip()
# Bucle principal del juego
reloj = pygame.time.Clock()
while True:
    manejar_eventos()
    teclas = pygame.key.get_pressed()
    # Duplicar velocidad si la tecla de control está presionada
    if teclas[pygame.K_LCTRL] or teclas[pygame.K_RCTRL]:
        velocidad = 60
    else:
        velocidad = 10
    mover_jugador(teclas)
    mover_jugador2(teclas)
    actualizar_pantalla()
    reloj.tick(15)