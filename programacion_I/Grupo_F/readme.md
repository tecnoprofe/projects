# PONG EXTREME
                                    UNIVERSIDAD PRIVADA DOMINGO SAVIO

                                FCT (FACULTAD DE CIENCIAS Y TECNOLOGIA)

![Alt text](LOGO_UNIVERSIDAD_PRIVADA.jpg)

                                          INGENIERIA DE SISTEMAS

                                      PONG EXTREME DE DOS JUGADORES

                                             INTEGRANTES:

                                      Mishele Ariany Tambaré Vaca

                                          Ronald Frias Guaristi

                                          Brayan Cuenca Tolavi

                                          Edwin Navia Ledezma

                                  DOCENTE: ING. JAIME ZAMBRANA CHACÓN

                                            Noviembre 2023

                                         Santa cruz – Bolivia

## RESUMEN
Pong Extreme es una versión moderna del clásico Pong para dos jugadores. Cada participante controla una raqueta, compitiendo por alcanzar 10 puntos al hacer pasar la pelota al campo del oponente. Destaca por su diseño visual atractivo, controles intuitivos y la aceleración progresiva de la pelota cada 15 segundos. Con fondos dinámicos y una presentación vibrante, ofrece una experiencia de juego clásico con un toque contemporáneo, ideal para competir de manera amena entre amigos o familiares.

![Alt text](Pong_Extreme-1.gif)


## ABSTRACT
Pong Extreme is a modern version of the classic Pong for two players. Each participant controls a racket, competing to reach 10 points by passing the ball into the opponent's court. It stands out for its attractive visual design, intuitive controls and the progressive acceleration of the ball every 15 seconds. With dynamic backgrounds and a vibrant presentation, it offers a classic gaming experience with a contemporary touch, ideal for entertaining competition among friends or family.

## DESARROLLO

### MARCO TEÓRICO

    PONG:
    Pong es un juego clásico de arcade que fue lanzado en 1972. Fue creado por Nolan Bushnell y es considerado uno de los primeros videojuegos populares. El juego simula un partido de tenis de mesa o ping-pong, de ahí su nombre.
    Pong fue fundamental en el desarrollo de la industria de los videojuegos y se convirtió en un icono de la cultura de los juegos electrónicos. Fue uno de los primeros juegos comerciales exitosos y contribuyó al surgimiento de la industria del entretenimiento digital que conocemos hoy en día.

    PYTHON:

    Python es un lenguaje de programación ampliamente utilizado en las aplicaciones web, el desarrollo de software, la ciencia de datos y el machine learning (ML). Los desarrolladores utilizan Python porque es eficiente y fácil de aprender, además de que se puede ejecutar en muchas plataformas diferentes. El software Python se puede descargar gratis, se integra bien a todos los tipos de sistemas y aumenta la velocidad del desarrollo.

    PYGAME:

    Pygame es un conjunto de módulos de Python diseñados para escribir videojuegos. Pygame agrega funcionalidad además de la excelente biblioteca SDL . Esto le permite crear juegos y programas multimedia con todas las funciones en el lenguaje Python.
    Pygame es muy portátil y se ejecuta en casi todas las plataformas y sistemas operativos.
    El propio Pygame se ha descargado millones de veces.
    Pygame es gratis. Lanzado bajo la licencia LGPL, puedes crear juegos de código abierto, freeware, shareware y comerciales con él. 

    SYS:

    Este módulo provee acceso a algunas variables usadas o mantenidas por el intérprete y a funciones que interactúan fuertemente con el intérprete. Siempre está disponible.

    OS:

    Este módulo provee una manera versátil de usar funcionalidades dependientes del sistema operativo. Si quieres leer o escribir un archivo mira open(), si quieres manipular rutas, mira el módulo os.path, y si quieres leer todas las líneas en todos los archivos en la línea de comandos mira el módulo fileinput. Para crear archivos temporales y directorios mira el módulo tempfile, y para el manejo de alto nivel de archivos y directorios puedes ver el módulo shutil.


### PROYECTO

#### librerias
    -pygame (versión 2.5.2) : Biblioteca para el desarrollo de videojuegos en Python. Se emplea para manejar la interfaz gráfica, eventos del teclado y la animación de elementos en la pantalla.

    -sys (versión estándar de Python) : módulo integrado en Python que proporciona acceso a algunas variables utilizadas o mantenidas por el intérprete y funciones que interactúan con el intérprete.

    -random (versión estándar de Python) : Módulo para generar números pseudoaleatorios.

    -os (versión estándar de Python) : módulo que proporciona una interfaz para interactuar con el sistema operativo, utilizado aquí para manejar rutas de archivos.

#### Diseño
    "Pong Extreme de Dos Jugadores" es una versión moderna del clásico juego de Pong diseñada para dos jugadores. Los participantes controlan raquetas en lados opuestos de la pantalla, compitiendo por alcanzar 10 puntos al hacer pasar una pelota al campo del oponente. El juego presenta un diseño visual atractivo con fondos dinámicos y animaciones vibrantes. Los controles son intuitivos, utilizando teclas específicas para mover las raquetas. Una característica única es la aceleración progresiva de la pelota cada 15 segundos, desafiando a los jugadores a adaptarse a la velocidad creciente. Con una presentación vibrante y mecánicas emocionantes, "Pong Extreme de Dos Jugadores" ofrece una experiencia de juego clásico con un toque contemporáneo, ideal para la competencia amistosa entre amigos o familiares.



#### Funcionalidades

                                                 MANEJO DE PYGAME:

    pygame.init(): Inicializa la biblioteca Pygame.
    pygame.display.set_mode(): Configura el modo de visualización de la pantalla del juego.
    pygame.display.set_caption(): Establece el título de la ventana del juego.
                                
                                                  DISEÑO GRÁFICO:

    Carga de imágenes: Utiliza Pygame para cargar imágenes de fondo para la pantalla de juego, la pantalla de inicio y la pantalla de instrucciones.
    Dibujo de Elementos: Utiliza funciones de Pygame para dibujar raquetas, pelotas y elementos visuales en la pantalla.

                                                CONTROL DE EVENTOS:

    pygame.event.get(): Maneja eventos, como cierre de ventana y clics de ratón.
    Control de teclado: Utilice las teclas W, S y las flechas arriba y abajo para controlar las raquetas y responder a eventos del teclado.

                                                  LÓGICA DEL JUEGO:

    Movimiento de Pelotas: Funciones como mover_pelotas()controlar el movimiento de las pelotas en la pantalla, gestionando rebotes y colisiones.
    Puntuación: Lleva un seguimiento del puntaje de cada jugador y muestra el ganador cuando se alcanza un puntaje máximo.

                                                    TEMPORIZADOR:

    pygame.time.Clock(): Crea un objeto reloj para controlar la velocidad de actualización del juego.
    Temporizador: Controla el tiempo transcurrido en el juego para eventos como el aumento gradual de la velocidad de la pelota.

                                                FUNCIONES DE PANTALLA:

    Pantalla de Inicio e Instrucciones: Muestra pantallas informativas y responde a un clic del ratón para avanzar.
    Pantalla de juego: Muestra el fondo, las raquetas, las pelotas y la puntuación durante el juego.
    Mensajes de Ganador: Muestra un mensaje cuando un jugador alcanza el puntaje máximo.

                                                AUMENTO DE VELOCIDAD:

    aumentar_velocidad(): Incrementa gradualmente la velocidad de la pelota cada cierto intervalo de tiempo, añadiendo emoción al juego.

#### Código Fuente

    link del código fuente:
    
    https://github.com/MisheleTambare/PONK-EXTREME/blob/main/Pong_Extreme.py

## CONCLUSIÓN

    Pong Extreme ofrece una reinterpretación vibrante y emocionante del clásico juego de Pong, brindando una experiencia de juego moderna y atractiva. La implementación de la biblioteca Pygame posibilita la creación de una interfaz visual envolvente, destacando por sus fondos dinámicos, animaciones vibrantes y controles intuitivos. La adición de funciones únicas, como el aumento progresivo de la velocidad de la pelota cada 15 segundos, añade una capa adicional de desafío y emoción al juego, manteniendo a los jugadores comprometidos y alerta.

    Las múltiples pantallas, desde la pantalla de inicio hasta las instrucciones detalladas, contribuyen a una experiencia de usuario bien estructurada y accesible. Los controles simples, con teclas específicas para el movimiento de las raquetas, permiten una participación fácil para jugadores de todos los niveles.

    La combinación de elementos clásicos y modernos, como la mecánica de juego atemporal de Pong y las características visuales contemporáneas, hace que "Pong Extreme de Dos Jugadores" sea una opción divertida y social para competir entre amigos o familiares. La lógica del juego, desde el seguimiento de la puntuación hasta la gestión de eventos y la temporización, está implementada de manera efectiva para ofrecer una experiencia de juego fluida y cautivadora.

## BIBLIOGRAFÍA

    Chat GPT. (2020). Chat Openai. Obtenido de https://chat.openai.com/

    Danitic. (5 de mayo de 2020). YouTube. Obtenido de https://www.youtube.com/watch?v=TFATE5SCG2Q&list=PLs4HvJtsfmeNnK5c1s0XVRAUiQhXsTtmx

    PYTHON, A. (12 de octubre de 2021). YouTube. Obtenido de https://www.youtube.com/watch?v=zS7ALUB0iMo

