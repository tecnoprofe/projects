# PROYECTO FORMATIVO:  Sistema de Asistencia para la Panadería Victoria.
- LUIS ELMAR CHORE TOMICHA
- [Facebook](https://www.facebook.com/sergiomaronejr?mibextid=ZbWKwL)
- Ubicación: Santa Cruz de la Sierra

##	INTRODUCCION
El proyecto se centra en la implementación de un sistema de asistencia para la Panadería Victoria. Este sistema permitirá un registro eficiente de la entrada y salida de los empleados, contribuyendo así a la optimización de los recursos humanos y la mejora en la gestión del tiempo laboral.

##	OBJETIVOS
El objetivo principal es desarrollar un sistema robusto que incluya análisis, modelado, migraciones, modelos, sembrado de datos (seeder) y una API REST. Este sistema ayudará a la Panadería Victoria a llevar un control preciso de las horas laboradas por cada empleado, facilitando la gestión de vacaciones y permisos.

##	MARCO TEORICO


###	Laravel
Laravel es un framework de PHP para el desarrollo de aplicaciones web. Ofrece una estructura de código elegante y robusta, facilitando la implementación de autenticación, routing, sesiones y otras funcionalidades sin sacrificar la flexibilidad.

Sigue estos pasos para instalar Laravel en tu sistema:

1. **Instalar Composer**: Si aún no tienes Composer instalado, puedes descargarlo e instalarlo desde [getcomposer.org](https://getcomposer.org/).

2. **Crear un Nuevo Proyecto de Laravel**: Utiliza Composer para crear un nuevo proyecto de Laravel ejecutando el siguiente comando en tu terminal:

```bash
composer create-project --prefer-dist laravel/laravel nombre-del-proyecto
```



### METODOLOGIA

La metodología empleada para el desarrollo del sistema se basa en el modelo ágil de desarrollo de software, enfocándose en la entrega continua de valor a través de iteraciones cortas y feedback constante. Esto incluye:

1. **Análisis de Requerimientos:** Comprensión profunda de las necesidades de la Panadería Victoria y sus empleados.
2. **Diseño del Sistema:** Creación del diagrama de clases y definición de la base de datos y su estructura.
3. **Implementación:** Codificación del sistema utilizando Laravel y buenas prácticas de desarrollo.
4. **Pruebas:** Verificación de la funcionalidad a través de pruebas unitarias y de integración.
Despliegue: Implementación del sistema en un entorno de producción.


### CONSIGNA DEL PROBLEMA PLANTEADO

En una empresa panadera, se desea implementar un sistema de asistencia que 
permita llevar un registro de la entrada y salida de los empleados. 
Cada empleado tiene un número de identificación único, un nombre, 
un apellido, una fecha de nacimiento y una dirección. Además, cada 
empleado tiene un cargo, un turno de trabajo y puede tener asignados 
permisos para realizar ciertas acciones en el sistema. El sistema de 
asistencia debe permitir registrar la entrada y salida de cada empleado 
en un horario especifico, y se desea que el sistema calcule las horas 
trabajadas por cada empleado al final del dia. También se desea llevar 
un registro de las vacaciones de cada empleado.
Para diseñar el diagrama de clases de esta base de datos, podemos
identificar las siguientes clases:
Empleado: con los atributos de número de identificación, 
nombre,apellido, fecha de nacimiento y dirección, cargo y turno de 
trabajo. Permiso: con los atributos de nombre y descripción. Registro 
de asistencia: con los atributos de hora de entrada, hora de salida y 
horas trabajadas. Horario de trabajo: con los atributos de hora de 
entrada y hora de salida. Vacaciones: con los atributos de fecha de 
inicio y fecha de fin.
Además, podemos establecer las siguientes relaciones entre las
clases:
Un empleado puede tener varios permisos asignados. Cada empleado 
tiene varios registros de asistencia. Cada registro de asistencia 
pertenece a un empleado y a un horario de trabajo. Cada empleado 
puede tener varias fechas de vacaciones registradas.


##	MODELADO O SISTEMATIZACION
Se ha proporcionado un diagrama de clases detallado y se han descrito las migraciones necesarias para la creación de las tablas en la base de datos. Además, se han incluido modelos para cada entidad relevante en el sistema, tales como Empresa, Empleado, Cargo, entre otros. Esto establece una base sólida para el desarrollo y la implementación de la lógica de negocio.


### DIAGRAMA DE CLASES 

![diagrama](https://live.staticflickr.com/65535/53566353882_f84ae0da76.jpg)


### MIGRACION

![migracion](https://live.staticflickr.com/65535/53564253794_bc91546c1f.jpg)





```bash
<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //tabla Empresa
        Schema::create('empresa', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('direccion');
            $table->string('telefono');
            $table->timestamps();
            $table->softDeletes();
        });

        //tabla Empleado
        Schema::create('empleado', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('apellido');
            $table->date('fecha_de_nacimiento');
            $table->unsignedBigInteger('empresa_id');
            $table->foreign('empresa_id')->references('id')->on('empresa');
            $table->timestamps();
            $table->softDeletes();
        });

        //tabla Cargo
        Schema::create('cargo', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->timestamps();
            $table->softDeletes();
        });

        //tabla Turno
        Schema::create('turno', function (Blueprint $table) {
            $table->id();
            $table->string('nombre'); // mañana, tarde, noche
            $table->timestamps();
            $table->softDeletes();
        });

        //tabla Horario de Trabajo
        Schema::create('horario_de_trabajo', function (Blueprint $table) {
            $table->id();
            $table->time('entrada');
            $table->time('salida');
            $table->unsignedBigInteger('turno_id');
            $table->foreign('turno_id')->references('id')->on('turno');
            $table->timestamps();
            $table->softDeletes();
        });

        //tabla Permiso
        Schema::create('permiso', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('descripcion');
            $table->timestamps();
            $table->softDeletes();
        });

        //tabla Vacaciones
        Schema::create('vacaciones', function (Blueprint $table) {
            $table->id();
            $table->date('fecha_de_inicio');
            $table->date('fecha_de_fin');
            $table->unsignedBigInteger('empleado_id');
            $table->foreign('empleado_id')->references('id')->on('empleado');
            $table->timestamps();
            $table->softDeletes();
        });

        //tabla Asistencia
        Schema::create('asistencia', function (Blueprint $table) {
            $table->id();
            $table->dateTime('fecha_y_hora_de_entrada');
            $table->dateTime('fecha_y_hora_de_salida');
            $table->decimal('horas_trabajadas', 8, 2);
            $table->unsignedBigInteger('empleado_id');
            $table->foreign('empleado_id')->references('id')->on('empleado');
            $table->timestamps();
            $table->softDeletes();
        });

        // Relaciones entre Cargo y Empleado
        Schema::table('empleado', function (Blueprint $table) {
            $table->unsignedBigInteger('cargo_id')->after('empresa_id');
            $table->foreign('cargo_id')->references('id')->on('cargo');
        });

        // Relaciones entre Horario de Trabajo y Empleado
        Schema::table('empleado', function (Blueprint $table) {
            $table->unsignedBigInteger('horario_de_trabajo_id')->after('cargo_id');
            $table->foreign('horario_de_trabajo_id')->references('id')->on('horario_de_trabajo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empleado', function (Blueprint $table) {
            $table->dropForeign(['horario_de_trabajo_id']);
            $table->dropColumn('horario_de_trabajo_id');
        $table->dropForeign(['cargo_id']);
        $table->dropColumn('cargo_id');
    });

    Schema::dropIfExists('asistencia');
    Schema::dropIfExists('vacaciones');
    Schema::dropIfExists('permiso');
    Schema::dropIfExists('horario_de_trabajo');
    Schema::dropIfExists('turno');
    Schema::dropIfExists('cargo');
    Schema::dropIfExists('empleado');
    Schema::dropIfExists('empresa');
}
}

```

### MODELADO

![models](https://live.staticflickr.com/65535/53564368355_ce79abf9f1.jpg)

### Modelo de Empresa (Empresa.php):


```bash
<?php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Empresa extends Model
{
    protected $fillable=['nombre', 'direccion', 'telefono']; 
    use HasFactory;
    use SoftDeletes;
    protected $table = 'empresa';

    use HasFactory;



    public function empleados()
    {
        return $this->hasMany(Empleado::class);
    }

}


```

### Modelo de Empleado (Empleado.php):

```bash
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Empleado extends Model
{
    protected $fillable=['nombre', 'apellido', 'fecha_de_nacimiento', 'empresa_id', 'cargo_id', 'horario_de_trabajo_id']; 
    use HasFactory;
    use SoftDeletes;
    protected $table = 'empleado';



    public function empresas()
    {
        return $this->belongsTo(empresa::class);
    }

    public function cargos()
    {
        return $this->belongsTo(cargo::class);
    }

    public function horariosDeTrabajos()
    {
        return $this->belongsTo(horarioDeTrabajo::class);
    }

    public function vacaciones()
    {
        return $this->hasMany(Vacaciones::class);
    }

    public function asistencias()
    {
        return $this->hasMany(Asistencia::class);
    }
    public function empleados()
    {
        return $this->belongsTo(Empleado::class);
    }

    public function turnos()
    {
        return $this->belongsTo(Turno::class);
    }

}

```


### Modelo de Cargo (Cargo.php):

```bash
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Cargo extends Model
{

    protected $fillable= ['nombre']; 
    use HasFactory;
    use SoftDeletes;
    protected $table = 'cargo';

    public function empleados()
    {
        return $this->hasMany(Empleado::class);
    }
}


```

### Modelo de Turno (Turno.php):

```bash
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Turno extends Model
{
    protected $fillable= ['nombre']; 
    use HasFactory;
    use SoftDeletes;
    protected $table = 'turno';

   

    public function horarioDeTrabajo()
    {
        return $this->hasMany(horarioDeTrabajo::class);
    }
}

```

### Modelo de Horario de Trabajo (HorarioDeTrabajo.php):

```bash
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class HorarioDeTrabajo extends Model
{

    protected $fillable= ['entrada', 'salida', 'turno_id'
    ]; 
    use HasFactory;
    use SoftDeletes;
    protected $table = 'horario_de_trabajo';



    public function asistencia()
    {
        return $this->belongsTo(Asistencia::class);
    }

    public function empleados()
    {
        return $this->hasMany(Empleado::class);
    }
}

```

### Modelo de Permiso (Permiso.php):

```bash
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Permiso extends Model
{

    protected $fillable=['nombre', 'descripcion']; 
    use HasFactory;
    use SoftDeletes;
    protected $table = 'permiso';
    public function empleado()
    {
        return $this->belongsTo(Empleado::class);
    }
}


```

### Modelo de Vacaciones (Vacaciones.php):

```bash
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Vacaciones extends Model
{
    protected $fillable=['fecha_de_inicio', 'fecha_de_fin', 'empleado_id']; 
    use HasFactory;
    use SoftDeletes;
    protected $table = 'vacaciones';


 
    public function empleado()
    {
        return $this->belongsTo(Empleado::class);
    }
}

```

### Modelo de Asistencia (Asistencia.php):

```bash

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Asistencia extends Model
{
    protected $fillable=['fecha_y_hora_de_entrada', 'fecha_y_hora_de_salida', 'horas_trabajadas', 'empleado_id']; 
    use HasFactory;
    use SoftDeletes;
    protected $table = 'asistencia';

    public function empleado()
    {
        return $this->belongsTo(Empleado::class);
    }
    
    public function horariosdetrabajos()
    {
        return $this->belongsTo(HorarioDeTrabajo::class);
    }
}


```


### DATOS SEEDERS
Los datos seeder permitirán poblar la base de datos con información inicial necesaria para probar el sistema. Se han proporcionado ejemplos para la creación de empresas, empleados, cargos, y otros, lo que facilitará las fases de prueba y depuración del sistema.

![seeder](https://live.staticflickr.com/65535/53563071792_9c40cd15c5.jpg)

### SEEDER GENERAL
```bash
<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use  App\Models\Asistencia;
use  App\Models\Vacaciones;
use  App\Models\Permiso;
use  App\Models\HorarioDeTrabajo;
use  App\Models\Turno;
use  App\Models\Cargo;
use  App\Models\Empleado;
use  App\Models\Empresa;


class general extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //empresa
        Empresa::create([
            'nombre' => 'PANADERIA VICTORIA LTDA',
            'direccion' =>'Av. Brasil y Calle 1',
            'telefono' => '591 - 3 - 348-7070'
        ]);


      //cargo
        Cargo::create([
            'nombre' => 'Gerente General'
        ]);

        Cargo::create([
            'nombre' => 'Jefe de Produccion'
        ]);

        Cargo::create([
            'nombre' => 'Pastelero'
        ]);
        Cargo::create([
            'nombre' => 'Vendedor'
        ]);
        Cargo::create([
            'nombre' => 'Repartidor'
        ]);
        Cargo::create([
            'nombre' => 'Administrador de Inventario'
        ]);
        Cargo::create([
            'nombre' => 'Contador'
        ]);

        Cargo::create([
            'nombre' => 'Recursos Humanos'
        ]);

        Cargo::create([
            'nombre' => 'Secretaria'
        ]);

        Cargo::create([
            'nombre' => 'Analista de Sistema'
        ]);

        //turno

        Turno::create([
            'nombre' => 'Mañana'
        ]);

        Turno::create([
            'nombre' => 'Tarde'
        ]);

        Turno::create([
            'nombre' => 'Noche'
        ]);




       //horariodetrabajos
       HorarioDeTrabajo::create([
        'entrada' => '08:00:00',
        'salida' => '16:00:00',
        'turno_id' => '1',
    
    ]);
    HorarioDeTrabajo::create([
        'entrada' => '16:00:00',
        'salida' => '00:00:00',
        'turno_id' => '2',
    
    ]);
    HorarioDeTrabajo::create([
        'entrada' => '00:00:00',
        'salida' => '08:00:00',
        'turno_id' => '3',
    
    ]);
    HorarioDeTrabajo::create([
        'entrada' => '09:00:00',
        'salida' => '17:00:00',
        'turno_id' => '1',
    
    ]);

    HorarioDeTrabajo::create([
        'entrada' => '17:00:00',
        'salida' => '01:00:00',
        'turno_id' => '2',
    
    ]);

    HorarioDeTrabajo::create([
        'entrada' => '01:00:00',
        'salida' => '09:00:00',
        'turno_id' => '3',
    
    ]);
    HorarioDeTrabajo::create([
        'entrada' => '10:00:00',
        'salida' => '18:00:00',
        'turno_id' => '1',
    
    ]);
    HorarioDeTrabajo::create([
        'entrada' => '18:00:00',
        'salida' => '02:00:00',
        'turno_id' => '2',
    
    ]);

    HorarioDeTrabajo::create([
        'entrada' => '02:00:00',
        'salida' => '10:00:00',
        'turno_id' => '3',
    
    ]);
    HorarioDeTrabajo::create([
        'entrada' => '07:00:00',
        'salida' => '15:00:00',
        'turno_id' => '1',
    
    ]);


      //empleado
      Empleado::create([
        'nombre' => 'JUAN',
        'apellido' => 'MARTINEZ',
        'fecha_de_nacimiento' => '1990-05-15',
        'empresa_id' =>'1',
        'cargo_id' => '1',
        'horario_de_trabajo_id' => '1',

    ]);
      
    Empleado::create([
        'nombre' => 'MARIA',
        'apellido' => 'MENDEZ',
        'fecha_de_nacimiento' => '1985-08-15',
        'empresa_id' =>'1',
        'cargo_id' => '2',
        'horario_de_trabajo_id' => '2',

    ]);
    Empleado::create([
        'nombre' => 'CARLOS',
        'apellido' => 'BANZER',
        'fecha_de_nacimiento' => '2000-03-10',
        'empresa_id' =>'1',
        'cargo_id' => '3',
        'horario_de_trabajo_id' => '3',

    ]);
    Empleado::create([
        'nombre' => 'LAURA',
        'apellido' => 'RODRIGUEZ',
        'fecha_de_nacimiento' => '1994-11-30',
        'empresa_id' =>'1',
        'cargo_id' => '4',
        'horario_de_trabajo_id' => '1',

    ]);
    Empleado::create([
        'nombre' => 'STEFANY',
        'apellido' => 'VAZ',
        'fecha_de_nacimiento' => '1988-07-15',
        'empresa_id' =>'1',
        'cargo_id' => '5',
        'horario_de_trabajo_id' => '2',

    ]);
    Empleado::create([
        'nombre' => 'SERGIO',
        'apellido' => 'MARONE',
        'fecha_de_nacimiento' => '1997-02-28',
        'empresa_id' =>'1',
        'cargo_id' => '6',
        'horario_de_trabajo_id' => '3',

    ]);

Empleado::create([
        'nombre' => 'SERGIO',
        'apellido' => 'MARONE',
        'fecha_de_nacimiento' => '1998-06-18',
        'empresa_id' =>'1',
        'cargo_id' => '7',
        'horario_de_trabajo_id' => '1',

    ]);

Empleado::create([
        'nombre' => 'GUILHERME',
        'apellido' => 'WINTER',
        'fecha_de_nacimiento' => '1991-09-12',
        'empresa_id' =>'1',
        'cargo_id' => '8',
        'horario_de_trabajo_id' => '2',

    ]);

    Empleado::create([
        'nombre' => 'SOFIA',
        'apellido' => 'DIAZ',
        'fecha_de_nacimiento' => '1987-08-09',
        'empresa_id' =>'1',
        'cargo_id' => '9',
        'horario_de_trabajo_id' => '3',

    ]);
    Empleado::create([
        'nombre' => 'CLARA ',
        'apellido' => 'FLORES',
        'fecha_de_nacimiento' => '1999-02-06',
        'empresa_id' =>'1',
        'cargo_id' => '10',
        'horario_de_trabajo_id' => '1',

    ]);

 //asistencia
 Asistencia::create([
    'fecha_y_hora_de_entrada' => '2024-03-01 08:00:00',
    'fecha_y_hora_de_salida' => '2024-03-01 16:00:00',
    'horas_trabajadas' => '8.00',
    'empleado_id' => '1',
]);

Asistencia::create([
    'fecha_y_hora_de_entrada' => '2024-03-02 16:00:00',
    'fecha_y_hora_de_salida' => '2024-03-02 00:00:00',
    'horas_trabajadas' => '8.00',
    'empleado_id' => '2',
]);
    Asistencia::create([
        'fecha_y_hora_de_entrada' => '2024-03-03 00:00:00',
        'fecha_y_hora_de_salida' => '2024-03-03 08:00:00',
        'horas_trabajadas' => '8.00',
        'empleado_id' => '3',

]);


Asistencia::create([
    'fecha_y_hora_de_entrada' => '2024-03-04 09:00:00',
    'fecha_y_hora_de_salida' => '2024-03-04 17:00:00',
    'horas_trabajadas' => '8.00',
    'empleado_id' => '4',

]);

Asistencia::create([
'fecha_y_hora_de_entrada' => '2024-03-05 17:30:00',
'fecha_y_hora_de_salida' => '2024-03-05 01:00:00',
'horas_trabajadas' => '7.30',
'empleado_id' => '5',

]);


Asistencia::create([
'fecha_y_hora_de_entrada' => '2024-03-06 01:00:00',
'fecha_y_hora_de_salida' => '2024-03-06 09:00:00',
'horas_trabajadas' => '8.00',
'empleado_id' => '6',

]);

Asistencia::create([
'fecha_y_hora_de_entrada' => '2024-03-07 10:30:00',
'fecha_y_hora_de_salida' => '2024-03-07 18:30:00',
'horas_trabajadas' => '8.00',
'empleado_id' => '7',

]);

Asistencia::create([
'fecha_y_hora_de_entrada' => '2024-03-08 18:00:00',
'fecha_y_hora_de_salida' => '2024-03-08 02:00:00',
'horas_trabajadas' => '8.00',
'empleado_id' => '8',

]);

Asistencia::create([
'fecha_y_hora_de_entrada' => '2024-03-04 02:00:00',
'fecha_y_hora_de_salida' => '2024-03-04 10:00:00',
'horas_trabajadas' => '8.00',
'empleado_id' => '9',

]);
Asistencia::create([
'fecha_y_hora_de_entrada' => '2024-03-10 07:30:00',
'fecha_y_hora_de_salida' => '2024-03-10 15:00:00',
'horas_trabajadas' => '7.30',
'empleado_id' => '10',

]);


      //permiso
      Permiso::create([
        'nombre' => 'Permiso de Salida',
        'descripcion' => 'Permite al empleado salir del trabajo temprano'
    ]);

    Permiso::create([
        'nombre' => 'Permiso de salud',
        'descripcion' => 'Permiso por razones de salud'
    ]);
    Permiso::create([
        'nombre' => 'Permiso por estudios',
        'descripcion' => 'Permiso para continuar estudios'
    ]);
    Permiso::create([
        'nombre' => 'Permiso especial',
        'descripcion' => 'Permiso para situaciones especiales'
    ]);
    Permiso::create([
        'nombre' => 'Permiso de maternidad',
        'descripcion' => 'Permiso de maternidad'
    ]);

    Permiso::create([
        'nombre' => 'Permiso de paternidad',
        'descripcion' => 'Permiso por paternidad'
    ]);
    Permiso::create([
        'nombre' => 'Permiso de vacaciones',
        'descripcion' => 'Permiso para tomar vacaciones'
    ]);
    Permiso::create([
        'nombre' => 'Permiso de formación',
        'descripcion' => 'Permiso para formación laboral'
    ]);
    Permiso::create([
        'nombre' => 'Permiso de viaje',
        'descripcion' => 'Permiso para viajes de trabajo'
    ]);
    Permiso::create([
        'nombre' => 'Permiso administrativo',
        'descripcion' => 'Permiso por trámites administrativos'
    ]);

      //vacaciones
      Vacaciones::create([
        'fecha_de_inicio' => '2024-06-01',
        'fecha_de_fin' =>'2024-06-15',
        'empleado_id' => '1',
       
    ]);
    
    Vacaciones::create([
        'fecha_de_inicio' => '2024-07-01',
        'fecha_de_fin' =>'2024-07-15',
        'empleado_id' => '2',
       
    ]);
    
    Vacaciones::create([
        'fecha_de_inicio' => '2024-08-01',
        'fecha_de_fin' =>'2024-08-15',
        'empleado_id' => '3',
       
    ]);
    Vacaciones::create([
        'fecha_de_inicio' => '2024-09-01',
        'fecha_de_fin' =>'2024-09-15',
        'empleado_id' => '4',
       
    ]);

    Vacaciones::create([
        'fecha_de_inicio' => '2024-10-01',
        'fecha_de_fin' =>'2024-10-15',
        'empleado_id' => '5',
       
    ]);
    Vacaciones::create([
        'fecha_de_inicio' => '2024-11-01',
        'fecha_de_fin' =>'2024-11-15',
        'empleado_id' => '6',
       
    ]);

 Vacaciones::create([
        'fecha_de_inicio' => '2024-12-01',
        'fecha_de_fin' =>'2024-12-15',
        'empleado_id' => '7',
       
    ]);
    Vacaciones::create([
        'fecha_de_inicio' => '2025-01-01',
        'fecha_de_fin' =>'2025-01-15',
        'empleado_id' => '8',
       
    ]);
    Vacaciones::create([
        'fecha_de_inicio' => '2025-02-01',
        'fecha_de_fin' =>'2025-02-15',
        'empleado_id' => '9',
       
    ]);
    Vacaciones::create([
        'fecha_de_inicio' => '2025-03-01',
        'fecha_de_fin' =>'2025-03-15',
        'empleado_id' => '10',
       
    ]);
    }
}
```


### API.
La sección de la API detallará cómo interactuar con el sistema a través de HTTP, especificando los endpoints disponibles, los métodos de solicitud soportados (GET, POST, PUT, DELETE) y los formatos de los datos de respuesta. Integrar Swagger mejorará esta sección al proporcionar documentación interactiva y fácil de entender.

### API.PHP
```bash
<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


use Illuminate\Database\Seeder;
use  App\Models\Asistencia;
use  App\Models\Vacaciones;
use  App\Models\Permiso;
use  App\Models\HorarioDeTrabajo;
use  App\Models\Turno;
use  App\Models\Cargo;
use  App\Models\Empleado;
use  App\Models\Empresa;
use Database\Seeders\general;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//


Route::get('empresa', function () {
    return Empresa::all();
});


//total de empleados en la empresa
Route::get('empresas', function () {
    $empresas = Empresa::withCount('empleados')->get();
    return $empresas;
});


//EMPLEADOS

//todos los empleados
Route::get('empleado', function () {
    return Empleado::all();
});
// muestra los primero 5
Route::get('empleadolist', function () {
    return Empleado::take(5)->get();
});
// muestra campos especificos
Route::get('empleadocamp', function () {
    return Empleado::select('nombre', 'apellido', 'fecha_de_nacimiento', 'empresa_id', 'cargo_id', 'horario_de_trabajo_id')->get();
});
// muestra campos especificios en orden alfabetico
Route::get('empleadocampo', function () {
    return Empleado::select('nombre', 'apellido', 'fecha_de_nacimiento', 'empresa_id', 'cargo_id', 'horario_de_trabajo_id')
                    ->orderBy('nombre')
                    ->get();
});

  
//
Route::get('empleadotot', function () {
    $totalEmpleados = Empleado::count();
    return response()->json(['total_empleados' => $totalEmpleados]);
});


//empleados en orden alfabetico
Route::get('empleadoalfa', function () {
    return Empleado::orderBy('nombre')->get();
});

//empleados en ordes desc

Route::get('empleadodesc', function () {
    return Empleado::orderBy('nombre', 'desc')->get();
});

//empleados por id
Route::get('empleadoid', function () {
    return Empleado::findOrFail(3);
});
//empleado y el id de la empresa
Route::get('empleadoorg', function () {
    return Empleado::select('nombre', 'empresa_id')->get();
});

//empleado y el id de la empresa en orden alfa
Route::get('empleadoorgm', function () {
    return Empleado::select('nombre', 'empresa_id')->orderBy('nombre')->get();
});



//todas las asistencias
Route::get('asistencia', function () {
    return Asistencia::all();
});

Route::get('permiso', function () {
    return Permiso::all();
});

Route::get('cargo', function () {
    return Cargo::all();

    
});
Route::get('turno', function () {
    return Turno::all();



});Route::get('horariodetrabajo', function () {
    return HorarioDeTrabajo::all();
});


Route::get('vacaciones', function () {
    return Vacaciones::all();
});


```

##	CONCLUSIONES

El desarrollo de este sistema de asistencia para la Panadería Victoria demostrará cómo la tecnología puede optimizar la gestión del tiempo y recursos humanos en una empresa. Laravel se presenta como una herramienta poderosa y flexible para el desarrollo de aplicaciones web modernas, capaces de satisfacer las necesidades del negocio.
El diseño e implementación de un sistema de asistencia requerirá una planificación cuidadosa para abordar todos los aspectos mencionados anteriormente y garantizar un funcionamiento eficiente y confiable del sistema.

##	BIBLIOGRAFIA
- GESTION DE EVENTOS - https://gitlab.com/tecnoprofe/gestion-de-eventos
- PAGINA DE LARAVEL - https://laravel.com/
-  Laravel 8 Overview and Introduction to Jetstream - Livewire and Inertia,  https://www.youtube.com/watch?v=abcd1234

##	ANEXOS

### FOTOS 
![PANADERIA](https://live.staticflickr.com/65535/53560084198_8e8463a3a0.jpg)

![P1](https://live.staticflickr.com/65535/53560211884_77d794e58e.jpg)

![P2](https://live.staticflickr.com/65535/53560324835_8a98922704.jpg)

![P3](https://live.staticflickr.com/65535/53560211809_c95ca7fe1f.jpg)
![P4](https://live.staticflickr.com/65535/53559881886_b5a87e7ccd.jpg)

![P5](https://live.staticflickr.com/65535/53559025292_cceb34b8f3.jpg)

### UBICACION
- https://maps.app.goo.gl/uSMmys54vCkoX3er5

