# proyecto formativo: Modelado Backend de un sistema de gestion de ventas.
- Luis Jose Aramayo Tellez
- [Facebook](https://www.facebook.com/luis.aramayo.77?mibextid=ZbWKwL)

##	Introducción
este sistema esta creado para la gestion de venta de productos orientales , siendo los aceites prensados al frio los mas conocidos y lider en ventas en la empresa ITENEZ 

##	Objetivos
Dearrollar el Analisis, Modelado, Migraciones, Models, Seeder y la Api REst para la gestion de eventos sociales de la empresa SoniLum  de la ciudad de Santa Cruz de la Sierra. 

##	Marco Teórico
###	Laravel
Sigue estos pasos para instalar Laravel en tu sistema:

1. **Instalar Composer**: Si aún no tienes Composer instalado, puedes descargarlo e instalarlo desde [getcomposer.org](https://getcomposer.org/).

2. **Crear un Nuevo Proyecto de Laravel**: Utiliza Composer para crear un nuevo proyecto de Laravel ejecutando el siguiente comando en tu terminal:

```bash
composer create-project --prefer-dist laravel/laravel nombre-del-proyecto
```

###	MVC
###	Swagger para laravel
...
..
...

##	Metodología
Se describe detalladamente la metodología utilizada en el proyecto, incluyendo los pasos específicos seguidos para llevar a cabo la investigación o desarrollo.
##	Modelado o Sistematización
### Diagrama de clases
![diagrama](https://live.staticflickr.com/65535/53568694090_076309f17d.jpg)



### Migracion
```bash
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class General extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('clientes', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('apellido');
            $table->string('direccion');
            $table->string('telefono')->nullable();
            $table->string('email')->unique();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('empleados', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('apellido');
            $table->string('cargo');
            $table->string('departamento');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('categorias_producto', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->text('descripcion')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('proveedores', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('direccion');
            $table->string('telefono');
            $table->string('email')->unique();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('productos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->text('descripcion')->nullable();
            $table->decimal('precio', 8, 2);
            $table->foreignId('categoria_id')->constrained('categorias_producto');
            $table->foreignId('proveedor_id')->constrained('proveedores');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('pedidos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('cliente_id')->constrained('clientes');
            $table->foreignId('empleado_id')->nullable()->constrained('empleados');
            $table->datetime('fecha_pedido');
            $table->decimal('total', 8, 2);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('detalle_pedidos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('pedido_id')->constrained('pedidos');
            $table->foreignId('producto_id')->constrained('productos');
            $table->integer('cantidad');
            $table->decimal('precio', 8, 2);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('pagos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('pedido_id')->constrained('pedidos');
            $table->decimal('monto', 8, 2);
            $table->datetime('fecha_pago');
            $table->timestamps();
            $table->softDeletes();
        });

      
        Schema::create('metodos_pago', function (Blueprint $table) {
            $table->id();
            $table->string('descripcion');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('estados_pedido', function (Blueprint $table) {
            $table->id();
            $table->string('descripcion');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('historial_ventas', function (Blueprint $table) {
            $table->id();
            $table->foreignId('pedido_id')->constrained('pedidos');
            $table->datetime('fecha_venta');
            $table->decimal('monto_total', 8, 2);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('inventario', function (Blueprint $table) {
            $table->id();
            $table->foreignId('producto_id')->constrained('productos');
            $table->integer('cantidad_disponible');
            $table->string('ubicacion');
            $table->timestamps();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventario');
        Schema::dropIfExists('historial_ventas');
        Schema::dropIfExists('estados_pedido');
        Schema::dropIfExists('metodos_pago');
        Schema::dropIfExists('pagos');
        Schema::dropIfExists('detalle_pedidos');
        Schema::dropIfExists('pedidos');
        Schema::dropIfExists('productos');
        Schema::dropIfExists('proveedores');
        Schema::dropIfExists('categorias_producto');
        Schema::dropIfExists('empleados');
        Schema::dropIfExists('clientes');
    }
}


```
### Models
 ```bash
// cliente 
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class cliente extends Model
{   
    protected $fillable=['nombre', 'apellido', 'direccion', 'telefono', 'email']; 
    use HasFactory;
    use SoftDeletes;
    protected $table = 'clientes';
    use SoftDeletes;

   

    public function pedidos()
    {
        return $this->hasMany(Pedido::class);
    }
}
```
```bash

//empleado

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class empleado extends Model
{
    protected $fillable=['nombre', 'apellido', 'cargo', 'departamento']; 
    use HasFactory;
    use SoftDeletes;
    protected $table = 'empleados';
    use SoftDeletes;

    

    public function pedidos()
    {
        return $this->hasMany(Pedido::class);
    }
}
```
```bash

//categoria_producto

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CategoriaProducto extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['nombre', 'descripcion']; 
    protected $table = 'categorias_producto';

    public function productos()
    {
        return $this->hasMany(Producto::class);
    }
}

```
```bash

//proovedor

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class proveedor extends Model
{
    protected $fillable=['nombre', 'direccion', 'telefono', 'email']; 
    use HasFactory;
    use SoftDeletes;
    protected $table ='proveedores';
    use SoftDeletes;

    
    public function productos()
    {
        return $this->hasMany(Producto::class);
    }
}

```
```bash


//producto

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class producto extends Model
{
  
    protected $fillable=['nombre', 'descripcion', 'precio', 'categoria_id', 'proveedor_id']; 
    use HasFactory;
    use SoftDeletes;
    protected $table = 'productos';
    use SoftDeletes;

  

    public function categoria()
    {
        return $this->belongsTo(CategoriaProducto::class);
    }

    public function proveedor()
    {
        return $this->belongsTo(Proveedor::class);
    }

    public function detallePedidos()
    {
        return $this->hasMany(DetallePedido::class);
    }

    public function inventarios()
    {
        return $this->hasMany(Inventario::class);
    }
}

```
```bash

//pedido

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class pedido extends Model
{
    protected $fillable=['cliente_id', 'empleado_id', 'fecha_pedido', 'total']; 
    use HasFactory;
    use SoftDeletes;
    protected $table = 'pedidos';
    use SoftDeletes;

    

    public function cliente()
    {
        return $this->belongsTo(Cliente::class);
    }

    public function empleado()
    {
        return $this->belongsTo(Empleado::class);
    }

    public function detallePedidos()
    {
        return $this->hasMany(DetallePedido::class);
    }

    public function pagos()
    {
        return $this->hasMany(Pago::class);
    }
}

```
```bash
//detalle_pedido

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetallePedido extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'detalle_pedidos'; // Nombre de la tabla

    protected $fillable = ['pedido_id', 'producto_id', 'cantidad', 'precio'];

    public function pedido()
    {
        return $this->belongsTo(Pedido::class);
    }

    public function producto()
    {
        return $this->belongsTo(Producto::class);
    }
}
```
```bash


//pago
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pago extends Model
{
    use HasFactory;

    protected $fillable = ['pedido_id', 'monto', 'fecha_pago'];

    protected $table = 'pagos';
}

//metodopago

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MetodoPago extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['descripcion'];

    protected $table = 'metodos_pago';

    public $timestamps = false;
}

```
```bash
//estado_pedido
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EstadoPedido extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'estados_pedido';
    protected $fillable = ['descripcion'];
}
```
```bash

//historial_ventas

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistorialVentas extends Model
{
    use HasFactory;

    protected $fillable = ['pedido_id', 'fecha_venta', 'monto_total'];

    protected $table = 'historial_ventas';
}

```
```bash

//inventario

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inventario extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'inventario';

    protected $fillable = [
        'cantidad_disponible',
        'producto_id',
        'ubicacion'
    ];

    public function producto()
    {
        return $this->belongsTo(Producto::class);
    }
}
```





### Datos seeder
```bash
<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CategoriaProducto;
use App\Models\Proveedor;
use App\Models\MetodoPago;
use App\Models\EstadoPedido;
use App\Models\Cliente;
use App\Models\Empleado;
use App\Models\Producto;
use App\Models\Pedido;
use App\Models\DetallePedido;
use App\Models\Pago;
use App\Models\HistorialVentas;
use App\Models\Inventario;

class TablaGeneralSeeders extends Seeder
{
    public function run()
    {
        // Categorías de Producto
        $categoriasProducto = [
            ['nombre' => 'ACEITE', 'descripcion' => 'ACEITES PRENSADOS AL FRIO'],
            ['nombre' => 'VINAGRE', 'descripcion' => 'VINAGRES DE MUCHAS VARIEDADES'],
        ];
        CategoriaProducto::insert($categoriasProducto);

        // Proveedores
        $proveedores = [
            ['nombre' => 'Proveedor Aceite', 'direccion' => 'Calle Brannif, Santa Cruz', 'telefono' => '75036364', 'email' => 'proveedoraceite@gmail.com'],
            ['nombre' => 'Proveedor Vinagre', 'direccion' => 'Calle Brannif, Santa Cruz', 'telefono' => '65012121', 'email' => 'proveedorvinagre@example.com'],
        ];
        Proveedor::insert($proveedores);
     
        // Métodos de Pago
        $metodosPago = [
            ['descripcion' => 'Tarjeta de crédito'],
            ['descripcion' => 'Transferencia bancaria'],
            ['descripcion' => 'Efectivo'],
        ];
        MetodoPago::insert($metodosPago);

        
        // Estados de Pedido
        $estadosPedido = [
            ['descripcion' => 'Pendiente'],
            ['descripcion' => 'En Proceso'],
            ['descripcion' => 'Completado'],
        ];
        EstadoPedido::insert($estadosPedido);
        
        // Clientes
        $clientes = [
            ['nombre' => 'Alejandro', 'apellido' => 'Gomez', 'direccion' => 'Avenida Los Pinos', 'telefono' => '70050232', 'email' => 'alejandro@example.com'],
            ['nombre' => 'Beatriz', 'apellido' => 'Diaz', 'direccion' => 'Avenida Santos Dumont 4to Anillo', 'telefono' => '77685555', 'email' => 'beatriz@example.com'],
            ['nombre' => 'Carlos', 'apellido' => 'Pérez', 'direccion' => 'Barrio Toborochi ', 'telefono' => '60074560', 'email' => 'carlos@example.com'],
            ['nombre' => 'Daniela', 'apellido' => 'Lopez', 'direccion' => 'Barrio España', 'telefono' => '69023235', 'email' => 'daniela@example.com'],
            ['nombre' => 'Ernesto', 'apellido' => 'Martinez', 'direccion' => 'Barrio Brigida', 'telefono' => '75021300', 'email' => 'ernesto@example.com'],
        ];
        Cliente::insert($clientes);
        

        // Empleados
        $empleados = [
            ['nombre' => 'Fernando', 'apellido' => 'Nuñez', 'cargo' => 'Vendedor', 'departamento' => 'Ventas'],
            ['nombre' => 'Gloria', 'apellido' => 'Jimenez', 'cargo' => 'Vendedor', 'departamento' => 'Ventas'],
            ['nombre' => 'Héctor', 'apellido' => 'Torres', 'cargo' => 'Gerente', 'departamento' => 'Administración'],
            ['nombre' => 'Irene', 'apellido' => 'Ruiz', 'cargo' => 'Vendedor', 'departamento' => 'Ventas'],
            ['nombre' => 'Jorge', 'apellido' => 'Salinas', 'cargo' => 'Vendedor', 'departamento' => 'Ventas'],
        ];
        Empleado::insert($empleados);
        

        // Productos
        $productos = [
            [
                'nombre' => 'aceite de coco 1000ml',
                'descripcion' => 'aceite de coco prensado al frio ',
                'precio' => 250.00,
                'categoria_id' => 1, 
                'proveedor_id' => 1, 
            ],
            [
                'nombre' => 'Aceite De Ricino 1000ml',
                'descripcion' => 'prensado al frio',
                'precio' =>200.00,
                'categoria_id' => 1,
                'proveedor_id' => 1,
            ],
            [
                'nombre' => 'Aceite De Linaza 1000ml',
                'descripcion' => 'prensado al frio',
                'precio' => 200.00,
                'categoria_id' => 1, 
                'proveedor_id' => 1, 
            ],
            [
                'nombre' => 'Aceite de Cusi 1000ml ',
                'descripcion' => 'prensado al frio.',
                'precio' => 200.00,
                'categoria_id' => 1,
                'proveedor_id' => 1,
            ],
            [
                'nombre' => 'Aceite De Almendra 1000ml ',
                'descripcion' => 'prensado al frio.',
                'precio' => 250.00,
                'categoria_id' => 1,
                'proveedor_id' => 1, 
            ],
            [
                'nombre' => 'Vinagre De Manzana',
                'descripcion' => 'añejado',
                'precio' => 45.00,
                'categoria_id' => 2,
                'proveedor_id' => 2,
            ],
            [
                'nombre' => 'Vinagre De Limon ',
                'descripcion' => 'añejado.',
                'precio' => 40.00,
                'categoria_id' => 2,
                'proveedor_id' => 2,
            ],
            [
                'nombre' => 'Vinagre De Piña',
                'descripcion' => 'añejado',
                'precio' => 45.00,
                'categoria_id' => 2,
                'proveedor_id' => 2,
            ],
        ];
        Producto::insert($productos);

        //pedidos
        $pedidos = [
            ['cliente_id' => 1, 'empleado_id' => 1, 'fecha_pedido' => '2024-03-04 23:24:12', 'total' => 490.00],
            ['cliente_id' => 2, 'empleado_id' => 2, 'fecha_pedido' => '2024-03-04 23:24:12', 'total' => 300.00],
            ['cliente_id' => 3, 'empleado_id' => 3, 'fecha_pedido' => '2024-03-04 23:24:12', 'total' => 200.00],
            ['cliente_id' => 4, 'empleado_id' => 4, 'fecha_pedido' => '2024-03-04 23:24:12', 'total' => 150.00],
            ['cliente_id' => 5, 'empleado_id' => 5, 'fecha_pedido' => '2024-03-04 23:24:12', 'total' => 450.00],
        ];
        
        Pedido::insert($pedidos);

        

           // Seed para DetallePedidos
           $detallePedidos = [
            ['pedido_id' => 1, 'producto_id' => 1, 'cantidad' => 2, 'precio' => 250.00],
            ['pedido_id' => 2, 'producto_id' => 2, 'cantidad' => 1, 'precio' => 200.00],
            ['pedido_id' => 3, 'producto_id' => 3, 'cantidad' => 3, 'precio' => 200.00],
            ['pedido_id' => 4, 'producto_id' => 4, 'cantidad' => 1, 'precio' => 200.00],
            ['pedido_id' => 5, 'producto_id' => 5, 'cantidad' => 2, 'precio' => 250.00],
            ['pedido_id' => 5, 'producto_id' => 6, 'cantidad' => 1, 'precio' => 45.00],
            ['pedido_id' => 4, 'producto_id' => 7, 'cantidad' => 2, 'precio' => 40.00],
            ['pedido_id' => 3, 'producto_id' => 8, 'cantidad' => 1, 'precio' => 45.00],
        ];

        DetallePedido::insert($detallePedidos);


        // Pagos
        $pagos = [
            ['pedido_id' => 1, 'monto' => 490, 'fecha_pago' => now()],
            ['pedido_id' => 2, 'monto' => 300, 'fecha_pago' => now()],
            ['pedido_id' => 3, 'monto' => 600, 'fecha_pago' => now()],
            ['pedido_id' => 4, 'monto' => 280, 'fecha_pago' => now()],
            ['pedido_id' => 5, 'monto' => 495, 'fecha_pago' => now()],
        ];

        Pago::insert($pagos);


        // HistorialVentas
       


$historialesVentas = [
    ['pedido_id' => 1, 'fecha_venta' => now(), 'monto_total' => 490.00],
    ['pedido_id' => 2, 'fecha_venta' => now(), 'monto_total' => 300.00],
    ['pedido_id' => 3, 'fecha_venta' => now(), 'monto_total' => 200.00],
    ['pedido_id' => 4, 'fecha_venta' => now(), 'monto_total' => 150.00],
    ['pedido_id' => 5, 'fecha_venta' => now(), 'monto_total' => 450.00],
];

HistorialVentas::insert($historialesVentas);
       

        // Inventario
        $inventarios = [
            ['producto_id' => 1, 'cantidad_disponible' => 100, 'ubicacion' => 'Almacén Central'],
            ['producto_id' => 2, 'cantidad_disponible' => 100, 'ubicacion' => 'Almacén Central'],
            ['producto_id' => 3, 'cantidad_disponible' => 100, 'ubicacion' => 'Almacén Central'],
            ['producto_id' => 4, 'cantidad_disponible' => 100, 'ubicacion' => 'Almacén Central'],
            ['producto_id' => 5, 'cantidad_disponible' => 100, 'ubicacion' => 'Almacén Central'],
            ['producto_id' => 6, 'cantidad_disponible' => 50, 'ubicacion' => 'Almacén Secundario'],
            ['producto_id' => 7, 'cantidad_disponible' => 50, 'ubicacion' => 'Almacén Secundario'],
            ['producto_id' => 8, 'cantidad_disponible' => 50, 'ubicacion' => 'Almacén Secundario'],
           
        ]; 
        Inventario::insert($inventarios);
        

        
    }
    
}
```
### Database seeder


```bash

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        
           
            $this->call(TablaGeneralSeeders::class);
        
                
    }
}


```


### APi.

```bash
<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Models\Producto;
use App\Models\Venta;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Productos
Route::get('productos', function () {
    return Producto::all();
});

// Total de ventas
Route::get('total-ventas', function () {
    $totalVentas = Venta::count();
    return response()->json(['total_ventas' => $totalVentas]);
});

// Ventas
Route::get('ventas', function () {
    return Venta::all();
});

// Muestra las primeras 5 ventas
Route::get('ventas-primeras-5', function () {
    return Venta::take(5)->get();
});

// Muestra campos específicos de ventas
Route::get('ventas-campos-especificos', function () {
    return Venta::select('producto_id', 'cantidad', 'monto_total', 'fecha_venta')->get();
});

// Muestra ventas con campos específicos en orden alfabético
Route::get('ventas-campos-especificos-orden-alfabetico', function () {
    return Venta::select('producto_id', 'cantidad', 'monto_total', 'fecha_venta')
        ->orderBy('fecha_venta')
        ->get();
});

// Total de productos
Route::get('total-productos', function () {
    $totalProductos = Producto::count();
    return response()->json(['total_productos' => $totalProductos]);
});

// Productos en orden alfabético
Route::get('productos-orden-alfabetico', function () {
    return Producto::orderBy('nombre')->get();
});

// Productos en orden descendente
Route::get('productos-orden-descendente', function () {
    return Producto::orderBy('nombre', 'desc')->get();
});

// Producto por id
Route::get('producto-por-id', function () {
    return Producto::findOrFail(3);
});

// Producto y su precio
Route::get('producto-y-precio', function () {
    return Producto::select('nombre', 'precio')->get();
});

// Producto y su precio en orden alfabético
Route::get('producto-y-precio-orden-alfabetico', function () {
    return Producto::select('nombre', 'precio')->orderBy('nombre')->get();
});

// Ventas por rango de fecha
Route::get('ventas-por-fecha', function () {
    return Venta::whereDate('fecha_venta', '>=', now()->subDays(7))->get();
});

// Ventas por cantidad
Route::get('ventas-por-cantidad', function () {
    return Venta::where('cantidad', '>', 10)->get();
});


```


##	Conclusiones
se diseño un sitema de gestion de ventas para modernizar la empresa y no llevar registros por excel y asi tener una base de datos funcional .
##	Bibliografía
-  Laravel 8 Overview and Introduction to Jetstream - Livewire and Inertia,  https://www.youtube.com/watch?v=abcd1234
## foto empresa

![diagramaa](https://live.staticflickr.com/65535/53568450968_2b3d3d4b2c.jpg)
### ubicacion
SantosDumont entre 2do y 3er anillo 