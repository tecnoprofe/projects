# proyecto formativo:Sistema de Ventas de Autos
- Carlos Rafael Suarez Roda
- [TikTok](https://www.tiktok.com/@karlitossuarez?_t=8inBRhuincE&_r=1)

## Introducción
El Sistema de Ventas de Autos es un proyecto diseñado para gestionar el proceso de venta de automóviles en una concesionaria. Este sistema proporciona una plataforma centralizada para administrar el inventario de autos y realizar ventas a clientes.

## Objetivos
1. Desarrollar una plataforma que permita a los empleados de la concesionaria gestionar de manera eficiente el proceso de ventas de los Vehiculos.
2. Facilitar a los clientes la búsqueda y compra de los vehiculos disponibles en la concesionaria.
3. Mejorar la eficiencia y la precisión en el seguimiento de las transacciones de venta de automóviles.

## Marco Teórico
Un sistema de ventas de vehículos es una aplicación informática diseñada para gestionar eficientemente todas las actividades relacionadas con la comercialización de automóviles, desde la gestión del inventario hasta la atención al cliente y el seguimiento de las transacciones. Este marco teórico proporciona una visión general de los conceptos y tecnologías fundamentales que respaldan el desarrollo y funcionamiento de un sistema de ventas de vehículos.

## Metodología
El desarrollo del Sistema de Ventas de Autos se basó en la metodología de desarrollo ágil Scrum. Se realizaron reuniones regulares de planificación, revisión y retrospectiva para iterar y mejorar continuamente el sistema.

### Diagrama de clases
![diagrama](https://live.staticflickr.com/65535/53557713427_cee3ee88a7_h.jpg)


### Migracion
```bash
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->id();
            $table->string('nombre')->nullable();
            $table->string('apellido')->nullable();
            $table->string('direccion')->nullable();
            $table->string('email')->nullable();
            $table->string('telefono')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehiculosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehiculos', function (Blueprint $table) {
            $table->id();
            $table->string('modelo')->nullable();
            $table->integer('año')->nullable();
            $table->string('color')->nullable();
            $table->decimal('precio', 10, 2)->nullable();
            $table->string('marca')->nullable();
            $table->boolean('disponible')->default(true);
            $table->text('caracteristicas')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehiculos');
    }
}

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ventas', function (Blueprint $table) {
            $table->id();
            $table->foreignId('vehiculo_id')->constrained()->onDelete('cascade');
            $table->foreignId('cliente_id')->constrained()->onDelete('cascade');
            $table->dateTime('fecha');
            $table->decimal('monto', 10, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ventas');
    }
}

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendedoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendedores', function (Blueprint $table) {
            $table->id();
            $table->string('nombre')->nullable();
            $table->string('apellido')->nullable();
            $table->string('email')->nullable();
            $table->string('telefono')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendedores');
    }
}
```

## Modelado o Sistematización
El sistema se diseñó utilizando diagramas UML para representar la estructura de la base de datos, los casos de uso y los diagramas de secuencia para modelar la interacción entre los diferentes componentes del sistema.

## Conclusiones
El Sistema de Ventas de Autos proporciona una solución eficiente y escalable para la gestión de ventas de automóviles en una concesionaria. Ha mejorado significativamente la eficiencia operativa y la satisfacción del cliente.

## Bibliografía
- Martin, R.C. (2003). Agile Software Development, Principles, Patterns, and Practices. Pearson Education.
- Sommerville, I. (2016). Ingeniería de software. Pearson Educación.

## Anexos
Los anexos incluyen diagramas de base de datos, diagramas de clases UML y cualquier otro material técnico relevante para el proyecto.
