# proyecto formativo: Modelado Backend de un sistema de gestion de venta de libros.
- Ariel hoyos mita
- [Facebook](https://www.facebook.com/joseariel.hoyosmita.5?mibextid=ZbWKwL)

##	Introducción
El proyecto de gestión de venta de libros tiene como objetivo principal automatizar y facilitar las operaciones relacionadas con la administración de inventario, ventas y reseñas de una tienda de libros. Al implementar este sistema, se busca mejorar la eficiencia en la gestión de productos, pedidos y clientes, así como proporcionar una experiencia satisfactoria para los usuarios.

##	Objetivos
El proyecto tiene como meta principal desarrollar un sistema completo para la gestión de de venta de libros

Análisis de requisitos: Identificar las necesidades específicas de la venta de libros, como la gestión de libros, usuarios y préstamos.
Modelado de la base de datos: Diseñar una estructura de base de datos eficiente para almacenar información sobre libros, usuarios y transacciones de préstamo.
Implementación de migraciones: Utilizar Laravel para crear y ejecutar migraciones de bases de datos que establezcan la estructura necesaria.
Desarrollo de modelos: Crear modelos en Laravel que representen entidades clave, como libros, usuarios y préstamos, con relaciones adecuadas.
Creación de seeders: Generar datos de prueba para poblar la base de datos y facilitar las pruebas del sistema.
Creación de una API REST: Implementar una interfaz de programación de aplicaciones (API) REST utilizando Laravel para que los usuarios puedan acceder y manipular datos de la biblioteca de forma segura y eficiente.
##	Marco Teórico

###	Laravel
Sigue estos pasos para instalar Laravel en tu sistema:

1. **Instalar Composer**: Si aún no tienes Composer instalado, puedes descargarlo e instalarlo desde [getcomposer.org](https://getcomposer.org/).

2. **Crear un Nuevo Proyecto de Laravel**: Utiliza Composer para crear un nuevo proyecto de Laravel ejecutando el siguiente comando en tu terminal:

```bash
composer create-project --prefer-dist laravel/laravel nombre-del-proyecto
```
## Instalación y Creación del Proyecto
Para trabajar en este sistema, se utiliza Laravel. A continuación, se detallan los pasos para instalar Laravel en caso de no tenerlo previamente instalado y crear el proyecto:

1. **Instalar Composer**:
Si aún no tienes Composer instalado, descárgalo e instálalo desde [getcomposer.org](https://getcomposer.org/).

2. **Crear un Nuevo Proyecto de Laravel**:
Utiliza Composer para crear un nuevo proyecto de Laravel ejecutando el siguiente comando en tu terminal:

bash
composer create-project --prefer-dist laravel/laravel nombreProyecto

Este comando descargará e instalará la última versión estable de Laravel y sus dependencias.

3. **Configurar el Entorno de Desarrollo:**
Accede al directorio del proyecto y copia el archivo .env.example a .env. Luego, genera una nueva clave de aplicación ejecutando:

bash
php artisan key:generate

4. **Configurar la Base de Datos:**
Ajusta las configuraciones de la base de datos en el archivo .env y luego ejecuta las migraciones y seeders para preparar la base de datos:

bash
php artisan migrate --seed

Esto creará las tablas necesarias y poblará la base de datos con datos de prueba.

5. **Iniciar el Servidor de Desarrollo:**
Ejecuta el siguiente comando para iniciar el servidor de desarrollo:

bash
php artisan serve
Luego, accede en tu navegador a: (http://127.0.0.1:8000).

Con estos pasos, el proyecto estará creado y configurado para su uso.


##	Metodología
Se ha desarrollado un diagrama de base de datos detallado que define la estructura necesaria para gestionar la venta de libros. Este diagrama incluye tablas para libros, categorías, usuarios, pedidos, pagos, envíos, reseñas y más.

Además, se han creado migraciones en Laravel para generar las tablas correspondientes en la base de datos. Cada migración define la estructura de una tabla específica, incluyendo campos como nombre, descripción, precio, fecha, cantidad, entre otros, según las necesidades del sistema de venta de libros.

También se han implementado modelos en Laravel para cada entidad relevante en el sistema, como Libro, Categoría, Usuario, Pedido, Pago, Envío, Reseña, entre otros. Estos modelos proporcionan una interfaz para interactuar con los datos almacenados en la base de datos y establecen relaciones entre las diferentes entidades, lo que establece una base sólida para el desarrollo y la implementación de la lógica de negocio.
##	Modelado o Sistematización

### Diagrama de clases
![diagrama de clase del sistema de gestion de venta de libros](https://flic.kr/p/2pBN7Vc)

### Migracion
```bash
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class General extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       // Migration for table 'categoria'
        Schema::create('categoria', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->text('descripcion')->nullable();
            $table->timestamps();
            $table->softDeletes();        
        });

        // Migration for table 'libro'
        Schema::create('libro', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->text('descripcion');
            $table->decimal('precio', 8, 2);
            $table->unsignedBigInteger('categoria_id');
            $table->foreign('categoria_id')->references('id')->on('categoria');
            $table->timestamps();
            $table->softDeletes();
        });
                // Migration for table 'compra'
        Schema::create('compra', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('usuario_id');
            $table->foreign('usuario_id')->references('id')->on('usuario');
            $table->timestamp('fecha');
            $table->timestamps();
            $table->softDeletes();
        });
        // Migration for table 'pedido'
        Schema::create('pedido', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('usuario_id');
            $table->foreign('usuario_id')->references('id')->on('usuario');
            $table->decimal('total', 8, 2);
            $table->string('estado');
            $table->text('direccion_envio');
            $table->timestamp('fecha_pedido');
            $table->timestamps();
            $table->softDeletes();
        });

        // Migration for table 'pago'
        Schema::create('pago', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('pedido_id');
            $table->foreign('pedido_id')->references('id')->on('pedido');
            $table->timestamp('fecha');
            $table->decimal('monto', 8, 2);
            $table->string('metodo_pago');
            $table->timestamps();
            $table->softDeletes();
        });

        // Migration for table 'envio'
        Schema::create('envio', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('pedido_id');
            $table->foreign('pedido_id')->references('id')->on('pedido');
            $table->string('estado');
            $table->timestamp('fecha_envio');
            $table->text('direccion_envio');
            $table->timestamps();
            $table->softDeletes();
        });

        // Migration for table 'pedido_libro' 
        Schema::create('pedido_libro', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('pedido_id');
            $table->foreign('pedido_id')->references('id')->on('pedido');
            $table->unsignedBigInteger('libro_id');
            $table->foreign('libro_id')->references('id')->on('libro');
            $table->integer('cantidad');
            $table->timestamps();
            $table->softDeletes();
        });

        // Migration for table 'compra_libro' 
        Schema::create('compra_libro', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('compra_id');
            $table->foreign('compra_id')->references('id')->on('compra');
            $table->unsignedBigInteger('libro_id');
            $table->foreign('libro_id')->references('id')->on('libro');
            $table->integer('cantidad');
            $table->timestamps();
            $table->softDeletes();
        });
        // Migration for table 'reseña' 
        Schema::create('reseña', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('usuario_id');
            $table->foreign('usuario_id')->references('id')->on('usuario');
            $table->unsignedBigInteger('libro_id');
            $table->foreign('libro_id')->references('id')->on('libro');
            $table->integer('calificacion');
            $table->text('comentario');
            $table->timestamp('fecha');
            $table->timestamps();
            $table->softDeletes();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('reseña');        
        Schema::dropIfExists('compra_libro');
        Schema::dropIfExists('pedido_libro');
        Schema::dropIfExists('envio');
        Schema::dropIfExists('pago');
        Schema::dropIfExists('pedido');
        Schema::dropIfExists('compra');
        Schema::dropIfExists('libro');
        Schema::dropIfExists('categoria');
        //Schema::dropIfExists('usuario');
    }
}


```
### explicacion del un caso de modelado.
Relacion entre la tabla libros y la tabla pedidos.
Estas dos tablas tienen una relacion de Muchos a Muchos, en ese caso se procede a generar una tabla pivot llamada Pedido_libro. La logica de esto es detallar que los libros y los pedidos que se realizan en la venta teniendo la ids de las dos tablas
en el Model de estas dos tablas se tiene de la siguiente manera:
### Model Pedido
``` bash
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class pedido extends Model
{
    protected $fillable=['usuario_id','total','estado','direccion_envio','fecha_pedido']; 
    use HasFactory;
    use SoftDeletes;
    protected $table = 'pedido';

    public function usuario()
    {
        return $this->belongsToMany(usuario::class);
    }
    public function pago()
    {
        return $this->belongsTo(pago::class);
    }
    public function envio()
    {
        return $this->belongsTo(envio::class);
    }
    //public function productos()
    //{
      //  return $this->belongsToMany(producto::class,'orden_producto');
    //}
    public function libros()
    {
        return $this->belongsToMany(libro::class,'compra_libro')->withPivot('cantidad');
    }
}
```
### Model libro
``` bash
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class libro extends Model
{
    protected $fillable=['nombre','descripcion','precio','categoria_id']; 
    use HasFactory;
    use SoftDeletes;
    protected $table = 'libro';

    public function categoria()
    {
        return $this->belongsTo(categoria::class);
    }
    public function compras()
    {
        return $this->belongsToMany(compra::class, 'compra_libro');
    }
    public function pedidos()
    {
        return $this->belongsToMany(pedido::class, 'pedido_libro');
    }

    public function usuario()
    {
        return $this->belongsTo(libro::class,'reseña')->withPivot('calificacion','comentario','fecha');
    }
}

```
### Datos seeder
``` Bash
<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Categoria;
use App\Models\Libro;
use App\Models\Compra;
use App\Models\pedido;
use App\Models\Pago;
use App\Models\Envio;
use App\Models\PedidoLibro;
use App\Models\CompraLibro;
use App\Models\Reseña;
use App\Models\Usuario; // Corregido aquí

class generalventasLibros extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Crear algunos usuarios de ejemplo
        Usuario::create([
            'nombre' => 'Juan',
            'correo_electronico' => 'juan@example.com',
            'contraseña' => bcrypt('password123'),
            'direccion' => 'Calle Principal 123',
            'rol' => 'usuario'
        ]);

        Usuario::create([
            'nombre' => 'María',
            'correo_electronico' => 'maria@example.com',
            'contraseña' => bcrypt('password456'),
            'direccion' => 'Avenida Secundaria 456',
            'rol' => 'usuario'
        ]);

        Usuario::create([
            'nombre' => 'Pedro',
            'correo_electronico' => 'pedro@example.com',
            'contraseña' => bcrypt('password789'),
            'direccion' => 'Calle Principal 789',
            'rol' => 'admin'
        ]);
        // Crear algunas categorías
        $categoria1 = Categoria::create([
            'nombre' => 'Ficción',
            'descripcion' => 'Libros de ficción'
        ]);

        $categoria2 = Categoria::create([
            'nombre' => 'No ficción',
            'descripcion' => 'Libros de no ficción'
        ]);

        $categoria3 = Categoria::create([
            'nombre' => 'Fantasía',
            'descripcion' => 'Libros de fantasía'
        ]);

        // Crear algunos libros
        $libro1 = Libro::create([
            'nombre' => 'El señor de los anillos',
            'descripcion' => 'Una épica historia de fantasía.',
            'precio' => 20.00,
            'categoria_id' => $categoria1->id
        ]);

        $libro2 = Libro::create([
            'nombre' => 'Cien años de soledad',
            'descripcion' => 'Una novela clásica de realismo mágico.',
            'precio' => 15.00,
            'categoria_id' => $categoria1->id
        ]);

        $libro3 = Libro::create([
            'nombre' => 'Breve historia del tiempo',
            'descripcion' => 'Una exploración del universo y de los conceptos de la física teórica.',
            'precio' => 25.00,
            'categoria_id' => $categoria2->id
        ]);

        $libro4 = Libro::create([
            'nombre' => 'Nombre del cuarto libro',
            'descripcion' => 'Descripción del cuarto libro.',
            'precio' => 30.00,
            'categoria_id' => $categoria3->id
        ]);

        // Crear una compra
        $compra = Compra::create([
            'usuario_id' => 1,
            'fecha' => now()
        ]);

        $compra2 = Compra::create([
            'usuario_id' => 2,
            'fecha' => now()
        ]);
        
        $compra3 = Compra::create([
            'usuario_id' => 3,
            'fecha' => now()
        ]);

        // Agregar libros a la compra
        $compra->libros()->attach([
            $libro1->id => ['cantidad' => 2],
            $libro2->id => ['cantidad' => 1]
        ]);

        $compra2->libros()->attach([
            $libro1->id => ['cantidad' => 1],
            $libro3->id => ['cantidad' => 2]
        ]);
        
        $compra3->libros()->attach([
            $libro2->id => ['cantidad' => 3],
            $libro3->id => ['cantidad' => 1],
            $libro4->id => ['cantidad' => 2]
        ]);

        // Crear un pedido
        $pedido = Pedido::create([
            'usuario_id' => 2,
            'total' => 50.00,
            'estado' => 'En proceso',
            'direccion_envio' => 'Calle Principal 123',
            'fecha_pedido' => now()
        ]);

        $pedido2 = Pedido::create([
            'usuario_id' => 1,
            'total' => 40.00,
            'estado' => 'En proceso',
            'direccion_envio' => 'Calle Principal 456',
            'fecha_pedido' => now()
        ]);
        
        $pedido3 = Pedido::create([
            'usuario_id' => 3,
            'total' => 60.00,
            'estado' => 'En proceso',
            'direccion_envio' => 'Avenida Secundaria 789',
            'fecha_pedido' => now()
        ]);

        // Crear un pago para el pedido
        $pago = Pago::create([
            'pedido_id' => $pedido->id,
            'fecha' => now(),
            'monto' => $pedido->total,
            'metodo_pago' => 'Tarjeta de crédito'
        ]);

        $pago2 = Pago::create([
            'pedido_id' => $pedido2->id,
            'fecha' => now(),
            'monto' => $pedido2->total,
            'metodo_pago' => 'PayPal'
        ]);
        
        $pago3 = Pago::create([
            'pedido_id' => $pedido3->id,
            'fecha' => now(),
            'monto' => $pedido3->total,
            'metodo_pago' => 'Transferencia bancaria'
        ]);

        // Crear un envío para el pedido
        $envio = Envio::create([
            'pedido_id' => $pedido->id,
            'estado' => 'En camino',
            'fecha_envio' => now(),
            'direccion_envio' => $pedido->direccion_envio
        ]);

        $envio2 = Envio::create([
            'pedido_id' => $pedido2->id,
            'estado' => 'En preparación',
            'fecha_envio' => now(),
            'direccion_envio' => $pedido2->direccion_envio
        ]);
        
        $envio3 = Envio::create([
            'pedido_id' => $pedido3->id,
            'estado' => 'En espera',
            'fecha_envio' => now(),
            'direccion_envio' => $pedido3->direccion_envio
        ]);

        // Crear una reseña para un libro
        $reseña = Reseña::create([
            'usuario_id' => 1,
            'libro_id' => $libro1->id,
            'calificacion' => 5,
            'comentario' => '¡Una lectura increíble!',
            'fecha' => now()
        ]);

        $reseña2 = Reseña::create([
            'usuario_id' => 2,
            'libro_id' => $libro2->id,
            'calificacion' => 4,
            'comentario' => 'Interesante lectura, recomendado.',
            'fecha' => now()
        ]);
        
        $reseña3 = Reseña::create([
            'usuario_id' => 3,
            'libro_id' => $libro3->id,
            'calificacion' => 3,
            'comentario' => 'No fue lo que esperaba, pero tiene buen contenido.',
            'fecha' => now()
        ]);
    }
}

```
### APi.

```bash
<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\UserController;
use Database\Seeders\generalventasLibros;
use App\Models\Categoria;
use App\Models\Libro;
use App\Models\Compra;
use App\Models\pedido;
use App\Models\Pago;
use App\Models\Envio;
use App\Models\PedidoLibro;
use App\Models\CompraLibro;
use App\Models\Reseña;
use App\Models\Usuario;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('usuario', function(){ 
    return usuario::all();
});

Route::get('pedido', function(){ 
    return pedido::all();
});

Route::get('categoria', function(){ 
    return Categoria::all();
});

Route::get('compra', function(){ 
    return compra::find(1);
});

Route::get('envio', function(){ 
    return envio::all();
});

Route::get('libro', function(){ 
    return libro::all();
});

Route::get('pago', function(){ 
    return pago::all();
});

Route::get('reseña', function(){ 
    return reseña::all();
});

```

##	Conclusiones

El proyecto de Modelado Backend para un sistema de gestión de venta de libros se enfocó en automatizar y mejorar la eficiencia en la administración de inventario, ventas y reseñas de una tienda de libros. Utilizando el framework Laravel, se desarrolló una estructura sólida que incluye el modelado de la base de datos, migraciones, modelos y seeders. Además, se implementó una API REST para facilitar el acceso y manipulación de datos de manera segura. En resumen, el proyecto permitió adquirir habilidades clave en el desarrollo de sistemas backend utilizando tecnologías modernas y metodologías estructuradas.

##	Bibliografía

- GESTION DE EVENTOS - https://gitlab.com/tecnoprofe/gestion-de-eventos
- PAGINA DE LARAVEL - https://laravel.com/
-  Laravel 8 Overview and Introduction to Jetstream - Livewire and Inertia,  https://www.youtube.com/watch?v=abcd1234

##	Anexos
### foto de la empresa para la cual estan desarrollando

[venta de libros](https://flic.kr/p/2pBNrG7)
[venta de libros](https://flic.kr/p/2pBNrGc)
### ubicacion
Es ficticia