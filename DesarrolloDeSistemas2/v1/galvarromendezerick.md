﻿DOCENTE: ING. JAIME ZAMBRANA CHACON
ESTUDIANTE: ERICK GALVARRO MENDEZ
INSTAGRAM: https://www.instagram.com/mgalvarroerick?igsh=MW03aHIxYm9yenhqaA==

- **INTRODUCCION:**

Las ventas en línea han revolucionado la forma en que consumidores y empresas interactúan en el mercado global. Este proyecto se centra en el desarrollo de una base de datos robusta y escalable para gestionar un sistema de ventas en línea, abarcando desde la gestión de usuarios y productos hasta la logística de pedidos y envíos. La estructura de la base de datos ha sido diseñada para reflejar las operaciones típicas de un comercio electronico, permitiendo la administración eficiente de inventarios, la personalización de la experiencia de compra, y la optimización del proceso de verificar y entrega. 

La importancia de este proyecto radica en su capacidad para manejar transacciones complejas y voluminosas con precisión y velocidad, asegurando que los datos de los clientes se manejen de manera segura y que la información del inventario esté siempre actualizada. Esto no solo mejora la experiencia del usuario, sino que también proporciona a la empresa una herramienta poderosa para el análisis de datos, permitiendo ajustes estratégicos en la oferta de productos, la optimización de la cadena de suministro, y la implementación de campañas de marketing dirigidas. 

Al integrar tablas dedicadas para usuarios, productos, inventario, carritos de compra, órdenes de compra, pagos y envíos, la base de datos está preparada para facilitar todas las etapas de la jornada de compra en línea. La flexibilidad y escalabilidad del diseño garantizan que la base de datos pueda evolucionar junto con el crecimiento del negocio y las cambiantes demandas del mercado. En conjunto, esta base de datos es el núcleo sobre el cual se construye un sistema de ventas en línea eficiente, confiable y centrado en el usuario.

- **OBJETIVO GENERAL:**

El objetivo general de las ventas en línea es proporcionar una plataforma accesible y conveniente que permita a los consumidores realizar compras de bienes y servicios desde cualquier lugar y en cualquier momento, y a los vendedores expandir su alcance de mercado y operar de manera eficiente con menores costos de infraestructura física en comparación con las tiendas tradicionales. Esto busca maximizar las oportunidades de venta, mejorar la experiencia del cliente y aumentar la rentabilidad mediante el uso de tecnologías digitales para facilitar transacciones comerciales seguras y eficientes. Las ventas en línea también apuntan a ofrecer una amplia variedad de productos y servicios, proporcionar información detallada y comparativa a los consumidores, y adaptarse a la demanda del mercado en tiempo real con flexibilidad y dinamismo**.**

- **MARCO CONCEPTUAL:**

El objetivo general del proyecto de ventas en línea es desarrollar y desplegar una plataforma de comercio electrónico integral que brinde una experiencia de compra fluida y segura tanto para usuarios como para administradores. Esta plataforma estará centrada en una base de datos sólida y escalable, diseñada para manejar eficientemente todas las operaciones relacionadas con el comercio electrónico, incluyendo el manejo de inventarios, la gestión de usuarios, el procesamiento de pedidos, la realización de pagos y la coordinación de envíos.

La meta es crear un sistema que no solo simplifique el proceso de compra para el cliente final, sino que también proporcione a los administradores del sitio las herramientas necesarias para gestionar productos, inventarios, promociones y analíticas de venta de manera eficaz. Esto involucra la implementación de funcionalidades que permitan una actualización dinámica de inventarios, personalización de ofertas basada en comportamiento de usuario, y análisis detallado de datos para estrategias de mercado. 

El sistema estará diseñado para garantizar la escalabilidad, permitiendo que el negocio crezca sin que la plataforma se convierta en un cuello de botella. Esto significa que la base de datos y la infraestructura subyacente serán capaces de manejar un aumento en el número de transacciones y usuarios sin degradar el rendimiento o la experiencia del usuario. Además, el proyecto busca integrar prácticas de seguridad de datos de vanguardia para proteger la información sensible de los usuarios y cumplir con las regulaciones aplicables. Al hacerlo, la plataforma se esforzará por construir y mantener la confianza del cliente, lo cual es esencial en el entorno digital actual.

- **DESARROLLO DE SISTEMA:**

- DIAGRAMA UML

DIAGRAMA DE CLASE

TIENDAS ONLINE:
![alt text](<WhatsApp Image 2024-02-25 at 12.21.44-1.jpeg>)
![](Aspose.Words.28476c7d-b8fc-424b-9d18-e933684aa3c7.002.png)





- MIGRACIONES DE LARAVEL
```bash
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class General extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Otras tablas ya definidas anteriormente...

        // Tabla usuarios
        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments('id_usuario');
            $table->string('nombre', 255)->nullable(false);
            $table->string('email', 255)->unique()->nullable(false);
            $table->text('direccion_envio')->nullable(false);
            $table->timestamp('fecha_registro')->default(DB::raw('CURRENT_TIMESTAMP'));
        });

        // Tabla productos
        Schema::create('productos', function (Blueprint $table) {
            $table->increments('id_producto');
            $table->string('nombre', 255)->nullable(false);
            $table->text('descripcion')->nullable();
            $table->decimal('precio', 10, 2)->nullable(false);
            $table->timestamp('fecha_agregado')->default(DB::raw('CURRENT_TIMESTAMP'));
        });

        // Tabla inventario
        Schema::create('inventario', function (Blueprint $table) {
            $table->increments('id_inventario');
            $table->unsignedInteger('id_producto');
            $table->integer('cantidad')->default(0);
            $table->foreign('id_producto')->references('id_producto')->on('productos');
        });

        // Tabla carrito_compra
        Schema::create('carrito_compra', function (Blueprint $table) {
            $table->increments('id_carrito');
            $table->unsignedInteger('id_usuario');
            $table->timestamp('fecha_creacion')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->foreign('id_usuario')->references('id_usuario')->on('usuarios');
        });

        // Tabla detalles_carrito
        Schema::create('detalles_carrito', function (Blueprint $table) {
            $table->increments('id_detalle');
            $table->unsignedInteger('id_carrito');
            $table->unsignedInteger('id_producto');
            $table->integer('cantidad');
            $table->foreign('id_carrito')->references('id_carrito')->on('carrito_compra');
            $table->foreign('id_producto')->references('id_producto')->on('productos');
        });

        // Tabla ordenes_compra
        Schema::create('ordenes_compra', function (Blueprint $table) {
            $table->increments('id_orden');
            $table->unsignedInteger('id_usuario');
            $table->timestamp('fecha_orden')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('estado', 255)->default('pendiente');
            $table->foreign('id_usuario')->references('id_usuario')->on('usuarios');
        });

        // Tabla detalles_orden
        Schema::create('detalles_orden', function (Blueprint $table) {
            $table->increments('id_detalle_orden');
            $table->unsignedInteger('id_orden');
            $table->unsignedInteger('id_producto');
            $table->integer('cantidad');
            $table->decimal('precio', 10, 2)->nullable(false);
            $table->foreign('id_orden')->references('id_orden')->on('ordenes_compra');
            $table->foreign('id_producto')->references('id_producto')->on('productos');
        });

        // Tabla pagos
        Schema::create('pagos', function (Blueprint $table) {
            $table->increments('id_pago');
            $table->unsignedInteger('id_orden');
            $table->decimal('monto', 10, 2)->nullable(false);
            $table->timestamp('fecha_pago')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('metodo', 255)->nullable(false);
            $table->string('estado', 255)->default('pendiente');
            $table->foreign('id_orden')->references('id_orden')->on('ordenes_compra');
        });

        // Tabla envios
        Schema::create('envios', function (Blueprint $table) {
            $table->increments('id_envio');
            $table->unsignedInteger('id_orden');
            $table->text('direccion_envio')->nullable(false);
            $table->timestamp('fecha_envio')->nullable();
            $table->string('estado', 255)->default('pendiente');
            $table->foreign('id_orden')->references('id_orden')->on('ordenes_compra');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('envios');
        Schema::dropIfExists('pagos');
        Schema::dropIfExists('detalles_orden');
        Schema::dropIfExists('ordenes_compra');
        Schema::dropIfExists('detalles_carrito');
        Schema::dropIfExists('carrito_compra');
        Schema::dropIfExists('inventario');
        Schema::dropIfExists('productos');
        Schema::dropIfExists('usuarios');
        
    }
}
```

- MODELOS
```bash
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table = 'usuarios';
    protected $primaryKey = 'id_usuario';
    public $timestamps = false;

    protected $fillable = [
        'nombre', 'email', 'direccion_envio', 'fecha_registro',
    ];

    // Relación con el carrito de compra
    public function carritoCompra()
    {
        return $this->hasMany('App\CarritoCompra', 'id_usuario', 'id_usuario');
    }

    // Relación con las órdenes de compra
    public function ordenesCompra()
    {
        return $this->hasMany('App\OrdenCompra', 'id_usuario', 'id_usuario');
    }
}
```

```bash
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $table = 'productos';
    protected $primaryKey = 'id_producto';
    public $timestamps = false;

    protected $fillable = [
        'nombre', 'descripcion', 'precio', 'fecha_agregado',
    ];

    // Relación con el inventario
    public function inventario()
    {
        return $this->hasOne('App\Inventario', 'id_producto', 'id_producto');
    }

    // Relación con los detalles de carrito
    public function detallesCarrito()
    {
        return $this->hasMany('App\DetalleCarrito', 'id_producto', 'id_producto');
    }

    // Relación con los detalles de orden
    public function detallesOrden()
    {
        return $this->hasMany('App\DetalleOrden', 'id_producto', 'id_producto');
    }
}
```

```bash
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventario extends Model
{
    protected $table = 'inventario';
    protected $primaryKey = 'id_inventario';
    public $timestamps = false;

    protected $fillable = [
        'id_producto', 'cantidad',
    ];

    // Relación con el producto
    public function producto()
    {
        return $this->belongsTo('App\Producto', 'id_producto', 'id_producto');
    }
}
```

```bash
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarritoCompra extends Model
{
    protected $table = 'carrito_compra';
    protected $primaryKey = 'id_carrito';
    public $timestamps = false;

    protected $fillable = [
        'id_usuario', 'fecha_creacion',
    ];

    // Relación con el usuario
    public function usuario()
    {
        return $this->belongsTo('App\Usuario', 'id_usuario', 'id_usuario');
    }

    // Relación con los detalles del carrito
    public function detallesCarrito()
    {
        return $this->hasMany('App\DetalleCarrito', 'id_carrito', 'id_carrito');
    }
}
```

```bash
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleCarrito extends Model
{
    protected $table = 'detalles_carrito';
    protected $primaryKey = 'id_detalle';
    public $timestamps = false;

    protected $fillable = [
        'id_carrito', 'id_producto', 'cantidad',
    ];

    // Relación con el carrito de compra
    public function carritoCompra()
    {
        return $this->belongsTo('App\CarritoCompra', 'id_carrito', 'id_carrito');
    }

    // Relación con el producto
    public function producto()
    {
        return $this->belongsTo('App\Producto', 'id_producto', 'id_producto');
    }
}
```

```bash
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrdenCompra extends Model
{
    protected $table = 'ordenes_compra';
    protected $primaryKey = 'id_orden';
    public $timestamps = false;

    protected $fillable = [
        'id_usuario', 'fecha_orden', 'estado',
    ];

    // Relación con el usuario
    public function usuario()
    {
        return $this->belongsTo('App\Usuario', 'id_usuario', 'id_usuario');
    }

    // Relación con los detalles de orden
    public function detallesOrden()
    {
        return $this->hasMany('App\DetalleOrden', 'id_orden', 'id_orden');
    }

    // Relación con los pagos
    public function pagos()
    {
        return $this->hasOne('App\Pago', 'id_orden', 'id_orden');
    }

    // Relación con los envíos
    public function envio()
    {
        return $this->hasOne('App\Envio', 'id_orden', 'id_orden');
    }
}
```

```bash
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleOrden extends Model
{
    protected $table = 'detalles_orden';
    protected $primaryKey = 'id_detalle_orden';
    public $timestamps = false;

    protected $fillable = [
        'id_orden', 'id_producto', 'cantidad', 'precio',
    ];

    // Relación con la orden de compra
    public function ordenCompra()
    {
        return $this->belongsTo('App\OrdenCompra', 'id_orden', 'id_orden');
    }

    // Relación con el producto
    public function producto()
    {
        return $this->belongsTo('App\Producto', 'id_producto', 'id_producto');
    }
}
```

```bash
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pago extends Model
{
    protected $table = 'pagos';
    protected $primaryKey = 'id_pago';
    public $timestamps = false;

    protected $fillable = [
        'id_orden', 'monto', 'fecha_pago', 'metodo', 'estado',
    ];

    // Relación con la orden de compra
    public function ordenCompra()
    {
        return $this->belongsTo('App\OrdenCompra', 'id_orden', 'id_orden');
    }
}
```

```bash
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Envio extends Model
{
    protected $table = 'envios';
    protected $primaryKey = 'id_envio';
    public $timestamps = false;

    protected $fillable = [
        'id_orden', 'direccion_envio', 'fecha_envio', 'estado',
    ];

    // Relación con la orden de compra
    public function ordenCompra()
    {
        return $this->belongsTo('App\OrdenCompra', 'id_orden', 'id_orden');
    }
}
```


- **CONCLUSIONES:**

el sistema de ventas en línea desarrollado representa un paso significativo hacia adelante en el comercio electrónico, proporcionando un valioso aprendizaje para proyectos futuros y estableciendo nuevos estándares en la industria para la integración de tecnología, diseño y estrategia comercial.

- **BIBLIOGRAFIA:**

- Kim, W., & Mauborgne, R. (2015). Blue Ocean Strategy, Expanded Edition: How to Create Uncontested Market Space and Make the Competition Irrelevant. Harvard Business Review Press. - Aunque no se centra específicamente en el comercio electrónico, este libro ofrece estrategias para el desarrollo de mercados y productos, lo que puede ser útil para el aspecto estratégico de tu proyecto.:<https://www.ucipfg.com/Repositorio/GSPM/manuales/Estrategia_Oceano_Azul.pdf>

- Fisher, M., Raman, A., & McClelland, A. S. (2010). Rocket Science Retailing is Almost Here--Are You Ready? Harvard Business School Press. - Abarca la gestión de inventarios y la logística, que son componentes clave de tu sistema de ventas en línea.: <https://hbswk.hbs.edu/archive/rocket-science-retailing-is-almost-here-are-you-ready>

- **ANEXOS:**

![alt text](<WhatsApp Image 2024-02-23 at 11.41.44-2.jpeg>)



