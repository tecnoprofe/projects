# proyecto formativo: Modelado Backend de un sistema de gestion de gastos.
- Jose miguel fanola
- [Facebook](https://www.facebook.com/?locale=es_LA)
- [Gitlab](https://gitlab.com/)
## Sistema de Gestión de Gastos para Panadería Jenecheru
# Introducción
El sistema de gestión de gastos de la empresa Jenecheru ha sido desarrollado para optimizar y controlar eficientemente los procesos relacionados con la administración financiera. Este sistema facilita la supervisión y registro detallado de los gastos, proporcionando herramientas para un análisis exhaustivo.

# Objetivo General
* Desarrollar un sistema integral de gestión de gastos para la panadería Jenecheru, que permita registrar, supervisar y controlar de manera eficiente todos los aspectos relacionados con los gastos, incluyendo la gestión de productos, pago de sueldos a empleados y otros gastos operativos.

# Objetivos Específicos
 * Análisis de Requisitos:

 * Realizar un análisis exhaustivo de los requisitos específicos de la panadería en términos de gestión de gastos.
 - Modelado de Base de Datos:

 Diseñar una estructura de base de datos que refleje con precisión los diferentes aspectos de la gestión de gastos.
 Implementación de Migraciones:

 Implementar migraciones para configurar la base de datos de acuerdo con el modelo diseñado.
 - Creación de Modelos:

 Desarrollar modelos que representen cada entidad de la base de datos, facilitando su manipulación y gestión dentro del sistema.
 - Generación de Semillas:

 Generar semillas para poblar la base de datos con datos de prueba que reflejen escenarios realistas de gestión de gastos.
 - Desarrollo de API REST:

 Implementar una API REST que permita la interacción con el sistema de gestión de gastos, facilitando la integración con otras aplicaciones y servicios.
 # Marco Conceptual
 - PHP
 PHP (Hypertext Preprocessor) es un lenguaje de programación ampliamente utilizado en el desarrollo de aplicaciones web. Se utiliza en el lado del servidor para generar contenido dinámico y interactuar con bases de datos.

 Instalación de PHP
 Windows: Descargar el instalador de PHP desde php.net.
 Linux (Ubuntu): Ejecutar en la terminal:
 ```bash
 Copy code
 sudo apt update
 sudo apt install php
 Laravel
 Laravel es un framework de desarrollo de aplicaciones web escrito en PHP que sigue el patrón de arquitectura MVC.
 ```
 Instalación de Laravel
 Requisitos previos: Asegurarse de tener PHP y Composer instalados.
 Ejecutar en la terminal:
 ```bash
 composer global require laravel/installer
 ```
 Composer
 Composer es un administrador de paquetes para PHP que simplifica la gestión de dependencias en proyectos PHP.

 Instalación de Composer
 Windows: Descargar el instalador desde getcomposer.org.
 Linux (Ubuntu): Ejecutar en la terminal:
 ```bash
 sudo apt update
 sudo apt install php-cli unzip
 php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
 php composer-setup.php --install-dir=/usr/local/bin --filename=composer
 ```
# MySQL
 MySQL es un sistema de gestión de bases de datos relacional de código abierto.

# El proceso de análisis, diseño e implementación del sistema, incluyendo los diagramas UML
  * Digrama de clases
   ![diagrama](https://live.staticflickr.com/65535/53565380422_0b92b2df3b.jpg)
   
  * Digrama de entidad relacion
   ![digrama](https://live.staticflickr.com/65535/53566671785_d6b41cc4da.jpg)
   

# Migraciones de Laravel
 ```bash
 <?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        //cuentas
        Schema::create('caja_chicas', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->decimal('saldo', 10, 2);
            $table->timestamps();
        });

        //proveedores
        Schema::create('proveedores', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('contacto')->nullable();
            $table->timestamps();
            $table->softDeletes();            

        });

        //productos
        Schema::create('productos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->text('descripcion')->nullable();
            $table->decimal('precio', 10, 2);

            $table->unsignedBigInteger('proveedor_id');
            $table->foreign('proveedor_id')->references('id')->on('proveedores');
      
            $table->timestamps();
            $table->softDeletes();            

        });

        Schema::create('empleados', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('cargo');
            $table->decimal('salario', 10, 2);
            $table->date('fecha_ingreso');
            $table->timestamps();
        });

        //gastos_productos
        Schema::create('gastos_productos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('producto_id');
            $table->integer('cantidad');
            $table->decimal('total', 10, 2);
            $table->unsignedBigInteger('cajachica_id');
            $table->foreign('producto_id')->references('id')->on('productos');
            $table->foreign('cajachica_id')->references('id')->on('caja_chicas');
            $table->timestamps();
        });

        //gastos_sueldos
        Schema::create('gastos_sueldos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('empleado_id');
            $table->string('mes');
            $table->decimal('total', 10, 2);
            $table->unsignedBigInteger('cajachica_id');
            $table->foreign('empleado_id')->references('id')->on('empleados');
            $table->foreign('cajachica_id')->references('id')->on('caja_chicas');
            $table->timestamps();
        });

        // GastosPagadosProductos
        Schema::create('gastos_pagados_productos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('proveedor_id')->nullable();
            $table->unsignedBigInteger('gastosproductos_id');
            $table->date('fechaPago');
            $table->decimal('total', 10, 2);
            $table->unsignedBigInteger('cuenta_id');
            $table->foreign('proveedor_id')->references('id')->on('proveedores')->nullable();
            $table->foreign('gastosproductos_id')->references('id')->on('gastos_productos');
            $table->foreign('cuenta_id')->references('id')->on('caja_chicas');
            $table->timestamps();
        });

        // GastosPagadosSueldos
        Schema::create('gastos_pagados_sueldos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('empleado_id');
            $table->unsignedBigInteger('gastosueldos_id');
            $table->date('fechaPago');
            $table->decimal('total', 10, 2);
            $table->unsignedBigInteger('cuenta_id');
            $table->foreign('empleado_id')->references('id')->on('empleados');
            $table->foreign('gastosueldos_id')->references('id')->on('gastos_sueldos');
            $table->foreign('cuenta_id')->references('id')->on('caja_chicas');
            $table->timestamps();
        });

        Schema::create('gastos_pago_pendiente_sueldos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('empleado_id')->nullable();
            $table->unsignedBigInteger('gastosueldos_id')->nullable();
            $table->decimal('total', 10, 2);
            $table->unsignedBigInteger('cuenta_id');
            
            $table->foreign('empleado_id')->nullable()->references('id')->on('empleados');
            $table->foreign('gastosueldos_id')->nullable()->references('id')->on('gastos_sueldos');
            $table->foreign('cuenta_id')->references('id')->on('caja_chicas');
            
            $table->timestamps();
        });
        
        
        Schema::create('gastos_pago_pendiente_productos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('proveedor_id')->nullable();
            $table->unsignedBigInteger('gastosproductos_id')->nullable();
            $table->decimal('total', 10, 2);
            $table->unsignedBigInteger('cuenta_id');
            $table->foreign('proveedor_id')->references('id')->on('proveedores')->nullable();
            $table->foreign('gastosproductos_id')->references('id')->on('gastos_productos')->nullable();
            $table->foreign('cuenta_id')->references('id')->on('caja_chicas');
            $table->timestamps();
        });

        //gastos_saldo
        Schema::create('gastos_saldo', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('cajachica_id');
            $table->decimal('saldo', 10, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('gastos_pago_pendiente_sueldos');
        Schema::dropIfExists('gastos_pago_pendiente_productos');
        Schema::dropIfExists('gastos_pagados_productos');
        Schema::dropIfExists('gastos_pagados_sueldos');
        Schema::dropIfExists('gastos_sueldos');
        Schema::dropIfExists('gastos_productos');
        Schema::table('productos', function (Blueprint $table) {
            $table->dropForeign(['proveedor_id']);
        });
        Schema::dropIfExists('proveedores');
        Schema::dropIfExists('productos');
        Schema::dropIfExists('empleados');
        Schema::dropIfExists('caja_chicas');
        Schema::dropIfExists('gastos_saldo');
    }
};

 ```

# Modelos
 ```bash
 Modelos:

CajaChica

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CajaChica extends Model
{
    use HasFactory;
    protected $table = 'caja_chicas';
    protected $fillable = ['nombre', 'saldo'];

    public function gastosProductos()
    {
        return $this->hasMany(GastosProductos::class);
    }

    public function gastosSueldos()
    {
        return $this->hasMany(GastosSueldos::class);
    }

    public function gastosPagadosProducto()
    {
        return $this->hasMany(GastosPagadosProductos::class);
    }
    public function gastosPagadossueldo()
    {
        return $this->hasMany(GastosPagadosSueldos::class);
    }

    public function gastosPendientesProducto()
    {
        return $this->hasMany(GastosPagoPendienteProductos::class);
    }
    public function gastosPendientesSueldo()
    {
        return $this->hasMany(GastosPagoPendienteSueldo::class);
    }

    public function gastosSaldo()
    {
        return $this->hasOne(GastosSaldo::class);
    }
}


Empleado

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
    use HasFactory;
    protected $table = 'empleados';
    protected $fillable = ['nombre', 'cargo', 'salario', 'fecha_ingreso'];

    public function gastosSueldos()
    {
        return $this->hasMany(GastosSueldos::class);
    }
}



Producto

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Producto extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'productos'; // Nombre de la tabla correcto
    protected $fillable = ['nombre', 'descripcion', 'precio', 'proveedor_id'];

    public function proveedor()
    {
        return $this->belongsTo(Proveedor::class);
    }

    public function gastosProductos()
    {
        return $this->hasMany(GastosProductos::class);
    }
}


Proveedor

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Proveedor extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'proveedores'; // Nombre de la tabla correcto

    protected $fillable = ['nombre', 'contacto'];

    public function productos()
    {
        return $this->hasMany(Producto::class);
    }
}

GastosProductos
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GastosProductos extends Model
{
    use HasFactory;
    protected $table = 'gastos_productos';
    protected $fillable = ['producto_id', 'cantidad', 'total', 'cajachica_id'];

    public function producto()
    {
        return $this->belongsTo(Producto::class, 'producto_id');
    }

    public function cajaChica()
    {
        return $this->belongsTo(CajaChica::class, 'cajachica_id');
    }
}

GastosSueldo
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GastosSueldos extends Model
{
    use HasFactory;
    protected $table = 'gastos_sueldos';
    protected $fillable = ['empleado_id', 'mes', 'total', 'cajachica_id'];

    public function empleado()
    {
        return $this->belongsTo(Empleado::class,'empleado_id');
    }

    public function cajaChica()
    {
        return $this->belongsTo(CajaChica::class,'cajachica_id');
    }
}



<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GastosSueldos extends Model
{
    use HasFactory;
    protected $table = 'gastos_sueldos';
    protected $fillable = ['empleado_id', 'mes', 'total', 'cajachica_id'];

    public function empleado()
    {
        return $this->belongsTo(Empleado::class,'empleado_id');
    }

    public function cajaChica()
    {
        return $this->belongsTo(CajaChica::class,'cajachica_id');
    }

GastosPagadosSueldos


<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GastosPagadosSueldos extends Model
{
    use HasFactory;

    protected $table = 'gastos_pagados_sueldos';
    protected $fillable = ['empleado_id', 'gastosueldos_id', 'fechaPago', 'total', 'cuenta_id'];

    public function gastosSueldos()
    {
        return $this->belongsTo(GastosSueldos::class, 'gastosueldos_id');
    }

    public function empleado()
    {
        return $this->belongsTo(Empleado::class, 'empleado_id');
    }

    public function cajaChica()
    {
        return $this->belongsTo(CajaChica::class, 'cuenta_id');
    }
}


GastosPagoPendienteProductos
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GastosPagoPendienteProductos extends Model
{
    use HasFactory;

    protected $table = 'gastos_pago_pendiente_productos';
    protected $fillable = ['proveedor_id', 'gastosproductos_id', 'total', 'cuenta_id'];

    public function gastosProductos()
    {
        return $this->belongsTo(GastosProductos::class, 'gastosproductos_id');
    }

    public function proveedor()
    {
        return $this->belongsTo(Proveedor::class, 'proveedor_id')->withDefault();
    }

    public function cajaChica()
    {
        return $this->belongsTo(CajaChica::class, 'cuenta_id');
    }
}


GastosPagoPendienteSueldo
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GastosPagoPendienteSueldo extends Model
{
    use HasFactory;

    protected $table = 'gastos_pago_pendiente_sueldos';
    protected $fillable = ['empleado_id', 'gastosueldos_id', 'total', 'cuenta_id'];

    public function gastosSueldos()
    {
        return $this->belongsTo(GastosSueldos::class, 'gastosueldos_id');
    }

    public function empleado()
    {
        return $this->belongsTo(Empleado::class, 'empleado_id');
    }

    public function cajaChica()
    {
        return $this->belongsTo(CajaChica::class, 'cuenta_id');
    }
}

 ```

# Uso de php artisan tinker
  ```bash
   use App\Models\GastosPagadosProductos;
   use App\Models\Proveedor;

 echo "Lista de Proveedores Pagados:\n";

 $gastosProductos = GastosPagadosProductos::all();
 foreach ($gastosProductos as $gastoProducto) {
    echo "Proveedor: " . $gastoProducto->proveedor->nombre . ", Total Gastado: $" . $gastoProducto->total . "\n";
  };
   ```
  ![ejecucion](https://live.staticflickr.com/65535/53566236216_cf8c1b227c.jpg)
  
  ```bash
  use App\Models\GastosPagoPendienteProducto;
 use App\Models\Proveedor;

 echo "Lista de Proveedores Pendientes:\n";

 $gastosPendientes = GastosPagoPendienteProducto::all();
  foreach ($gastosPendientes as $gastoPendiente) {
    echo "Proveedor: " . ($gastoPendiente->proveedor ? $gastoPendiente->proveedor->nombre : "Proveedor no especificado") . ", Total Pendiente: $" . $gastoPendiente->total . "\n";
 };

  ```
 ![ejecucion](https://live.staticflickr.com/65535/53565380392_3d686fe2c1.jpg)

# Creación de la API REST
 ```bash
 <?php

use App\Models\CajaChica;
use App\Models\Proveedor;
use App\Models\Producto;
use App\Models\Empleado;
use App\Models\GastosProductos;
use App\Models\GastosSueldos;
use App\Models\GastosPagadosProductos;
use App\Models\GastosPagadosSueldos;
use App\Models\GastosSaldo;
use App\Models\GastosPagoPendienteSueldo;
use App\Models\GastosPagoPendienteProductos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('caja_chicas',function(){
    return CajaChica::all();
});
Route::get('empleados',function(){
    return Empleado::all();
});
Route::get('gastos_pagados_productos', function() {
    return GastosPagadosProductos::with('gastosProductos', 'proveedor', 'cajaChica')->get();
});

Route::get('gastos_pagados_sueldos', function() {
    return GastosPagadosSueldos::with('gastosSueldos', 'empleado', 'cajaChica')->get();
});

Route::get('gastos_pago_pendiente_productos', function() {
    return GastosPagoPendienteProductos::with('gastosProductos', 'proveedor', 'cajaChica')->get();
});
Route::get('gastos_pago_pendiente_sueldos', function() {
    return GastosPagoPendienteSueldo::with('gastosSueldos', 'empleado', 'cajaChica')->get();
});
Route::get('gastos_productos', function () {
    $gastosProductos = GastosProductos::with(['producto', 'cajaChica'])->get();
    return $gastosProductos;
});

Route::get('gastosaldo',function(){
    return GastosSaldo::all();
});
Route::get('gastos_sueldos',function(){
    $gastosSueldos = GastosSueldos::with(['empleado', 'cajaChica'])->get();
    return  $gastosSueldos;
});
Route::get('productos',function(){
    return Producto::all();
});
Route::get('proveedores',function(){
    return Proveedor::all();
});

 ```
# Modelado de la base de datos: 
 - [Base de datos](https://live.staticflickr.com/65535/53566694980_b5f4525a98.jpg)

 - Revision de  la integridad de la base de datos
 [Base de datos](https://live.staticflickr.com/65535/53566637174_4496c9168b.jpg)
 
# Conclusiones
En resumen, el proceso de desarrollo del sistema de gestión de gastos para una panadería utilizando tecnologías como PHP, Laravel, Composer y MySQL implica varios pasos clave:
Instalación de las tecnologías: Se debe instalar PHP, Laravel, Composer y MySQL en el entorno de desarrollo para comenzar a trabajar en el proyecto.
En conclusión, el desarrollo de un sistema de gestión de gastos para una panadería utilizando PHP, Laravel, Composer y MySQL implica una serie de pasos que van desde la instalación de las tecnologías hasta el modelado de la base de datos y la creación de datos de prueba. Es importante seguir una metodología de desarrollo adecuada para garantizar la eficiencia y la calidad del proyecto.

# Bibliografía
 - [PHP.net] (https://www.php.net/)
 - [Laravel] (https://laravel.com/)
 - [Composer] (https://getcomposer.org/)
 - [MySQL] (https://dev.mysql.com/downloads/)

##  Anexos
## foto de la empresa para la cual estan desarrollando
 - Panaderia Jenecheru
 - [Facebook](https://www.facebook.com/profile.php?id=100088905941905&locale=es_LA)

