# Proyecto de Venta "LICOTIENDA"
- Hermes Saavedra Encinas

- [Faceboook Licotienda](https://www.facebook.com/lico.tienda.todoexpress)

- [Faceboook HermesSaavedra](https://www.facebook.com/hermesmauricio.saavedraencinas.7/)

## Introducción

En el mundo de las tiendas, es súper importante ser eficientes y precisos. Si no sabes cuánto inventario tienes o no puedes atender bien a tus clientes, estás en problemas. Por eso, estoy trabajando en un proyecto genial para mejorar mi tienda de licores, Licotienda. Quiero que sea lo más organizada y eficaz posible para poder brindar el mejor servicio a nuestros clientes. ¡Esperamos que este proyecto nos ayude a alcanzar el éxito!

## Objetivos

1. Desarrollar un sistema intuitivo y fácil de usar para los clientes de la tienda.
2. Permitir a los administradores gestionar el inventario de productos de manera eficiente.
3. Mejorar la eficiencia en la gestión de pedidos y clientes.
4. Proporcionar una plataforma segura para realizar transacciones comerciales.

## Marco Teórico

Contexto del Mercado Minorista de Licores: Breve análisis del panorama actual del mercado de licores, destacando la competencia, las tendencias de consumo y las necesidades del cliente en este sector.

Importancia de la Gestión de Inventario: Explicación de por qué una gestión eficiente del inventario es crucial para el éxito de Licotienda, incluyendo la optimización de costos, la prevención de pérdidas y la mejora en la experiencia del cliente.

Atención al Cliente y Personalización: Enfoque en la relevancia de brindar un servicio al cliente excepcional y personalizado en la industria de licores, destacando cómo el sistema de gestión puede ayudar a recopilar y utilizar datos de los clientes para mejorar la experiencia de compra.

Patrón de Diseño MVC y Eficiencia del Desarrollo: Breve descripción del patrón de diseño Modelo-Vista-Controlador (MVC) y cómo su aplicación en el desarrollo del sistema de gestión puede mejorar la organización del código y facilitar futuras actualizaciones y expansiones.

Cumplimiento Normativo y Legal: Consideración de las regulaciones y leyes relacionadas con la venta de alcohol, asegurando que el sistema de gestión cumpla con los requisitos legales y ayude a Licotienda a mantenerse en conformidad con las normativas pertinentes.

## Metodología

El desarrollo de este proyecto sigue un enfoque iterativo e incremental. Se lleva a cabo en varias etapas, incluyendo análisis de requisitos, diseño, implementación, pruebas y despliegue. Se utilizan metodologías ágiles para facilitar la adaptación a los cambios y asegurar la entrega de un producto de calidad.

### Diagrama de clases
![diagrama](https://live.staticflickr.com/65535/53558797943_4ebeb084fd_h.jpg) 

### Migracion
```bash
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class General extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('productos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre')->nullable();
            $table->string('descripcion')->nullable();
            $table->decimal('precio', 10, 2)->nullable();
            $table->timestamps();
        });


        Schema::create('clientes', function (Blueprint $table) {
            $table->id();
            $table->string('nombre')->nullable();
            $table->string('direccion')->nullable();
            $table->string('telefono')->nullable();
            $table->timestamps();
        });


        Schema::create('pedidos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_cliente');
            $table->foreign('id_cliente')->references('id')->on('clientes');
            $table->dateTime('fecha_pedido');
            $table->timestamps();
        });

        
        Schema::create('detalles_pedido', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_pedido');
            $table->foreign('id_pedido')->references('id')->on('pedidos');
            $table->unsignedBigInteger('id_producto');
            $table->foreign('id_producto')->references('id')->on('productos');
            $table->integer('cantidad');
            $table->timestamps();
        });




        Schema::create('productos_pedidos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_pedido');
            $table->foreign('id_pedido')->references('id')->on('pedidos');
            $table->unsignedBigInteger('id_producto');
            $table->foreign('id_producto')->references('id')->on('productos');
            $table->integer('cantidad');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
{
        Schema::dropIfExists('product_sale');
        Schema::dropIfExists('sale');

        Schema::dropIfExists('detalles_pedido');

        Schema::dropIfExists('productos_pedidos');

        Schema::dropIfExists('pedidos');
        Schema::dropIfExists('clientes');

        Schema::dropIfExists('productos');

        Schema::dropIfExists('category');
    } 
}


```

## Modelado o Sistematización

El sistema se basa en un modelo de datos que incluye cinco tablas principales:

1. Productos
2. Clientes
3. Pedidos
4. Detalles de Pedido
5. Productos_Pedidos (Tabla intermedia para la relación muchos a muchos entre Productos y Pedidos)

El modelo sigue el patrón MVC, donde cada componente (Modelo, Vista, Controlador) se encarga de una parte específica del sistema.

## Conclusiones

El desarrollo de este sistema de gestión de tienda de licores ha permitido entender mejor los desafíos y requisitos específicos de este tipo de negocio. Se han identificado áreas de mejora y se han implementado soluciones para optimizar la operación y mejorar la experiencia del cliente.

## Bibliografía

-  Laravel 8 Overview and Introduction to Jetstream - Livewire and Inertia,  https://www.youtube.com/watch?v=abcd1234

## Anexos

- Diagramas UML: Dibujos de cómo funciona el sistema.
- Capturas de pantalla: Fotos de cómo se ve la página web.
- Manuales de usuario: Instrucciones para usar la página.
- Datos de prueba: Ejemplos de nombres de productos, clientes, etc.
- Referencias técnicas: Lista de libros o sitios web que ayudaron a hacer la página.
- Requisitos del sistema: Lista de lo que la página necesita para funcionar.
- Casos de uso: Ejemplos de cómo la gente podría usar la página.
- Código extra: Otros pedazos de código que no están en la página principal.

