# APLICACIÓN WEB DE ARRENDAMIENTO DE DEPARTAMENTOS

Con el presente proyecto se busca integrar los siguientes puntos:
Facilitar la búsqueda de departamentos: El objetivo principal de la aplicación web podría ser proporcionar a los usuarios una plataforma fácil de usar para buscar y encontrar departamentos disponibles para arrendar. Esto implica desarrollar una interfaz intuitiva y funcionalidades de búsqueda avanzada que permitan a los usuarios filtrar los resultados según sus preferencias.
Agilizar el proceso de arrendamiento: La aplicación web puede tener como objetivo simplificar y agilizar el proceso de arrendamiento de departamentos. Esto podría incluir la posibilidad de completar formularios de solicitud en línea, realizar pagos electrónicos, firmar contratos digitales y gestionar la documentación necesaria de manera eficiente.
Mejorar la comunicación entre propietarios e inquilinos: La aplicación web puede servir como un canal de comunicación efectivo entre los propietarios y los inquilinos. Esto podría incluir la capacidad de enviar mensajes, recibir notificaciones sobre pagos y vencimientos de contratos, y solicitar mantenimiento o reparaciones.


### Integrantes
Ximena Justiniano Lujan
Jorge Daniel Moron Avila
Gustavo Soto Pecho
