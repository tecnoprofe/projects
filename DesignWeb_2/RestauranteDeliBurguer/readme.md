﻿
# INFORME DEL PROYECTO “RESTAURANTE DELIBURGER”

# UNIVERSIDAD PRIVADA DOMINGO SAVIO.

![Facultad de Ingeniería UPDS - Santa Cruz | Santa Cruz de la Sierra](./README/logoUPDS.jpeg)
![Deliburger](./README/logoRes.png)

## FACULTAD: Ingenieria en sistemas

## ASIGNATORA: DISEÑO WEB 2.

### LICENCIADO: 
JAIME ZAMBRANA CHACON

### ALUMNOS: 
LEONARDO CARRILLO AÑEZ

DIEGO BENJAMIN VENEGAS MENDUIÑAS

CARLOS RAUL VERA CHYRIKINS

## RESUMEN:

Hemos creado una página web para una hamburguesería llamada deliburger,la página cuenta con 2 partes: Página Principal y Pagina de Menu.La Página Principal, le hemos creado un carrusel, con imágenes llamativas con hamburguesas, seguido hemos puesto una imagen del menú, agregamos un link de WhatsApp para medir los pedidos. También agregamos un mapa de Google Maps que indica donde queda el restaurante. En la segunda página del Menú, hemos implementado el menú de la hamburguesería usando checkboxs para filtrar los alimentos, y hemos creado cartas, dentro de las cartas hemos puesto una imagen y un botón modal que al presionar el botón modal se despliega un modal con información de la hamburguesa, ingredientes, calorías y precios.

## ABSTRACT:

We created a web site for a hamburger joint called deliburger, this page have two parts: Principal Page and the Menu Page. The Principal Page, we created it with a carousel, with interesting pictures with hamburgers, then we put a picture of our menu, then we add a WhatsApp’s link so we can manage the orders. We also add a Google’s mapp so we can let you know where the restaurant is. In the second page of our Menu, we have put our restaurant’s menu using checkboxes so we can strain the food. We also created cards, and inside the cards we put a picture and a modal button which when you press it it launch a modal with the information of the hamburger, the price, calories and ingredients.


## Marco Teórico: La hamburguesería DELIBURGUER y su Presencia en Línea

En el dinámico mundo de la gastronomía, las hamburgueserías han evolucionado no solo en términos de sabor, sino también en su presencia en línea. Un ejemplo destacado es DELIBURGUER,una hamburguesería con sede en Santa Cruz, fundada en 2010.Con más de una década de experiencia.DELIBURGER ha forjado una reputación solida ofreciendo artículos de calidad y brindando un servicio al cliente excepcional.

## 1\.-Diseño Web y Experiencia del Usuario(1. UX/UI):

El sitio web de DELIBURGUER refleja un diseño cuidadoso y una atención meticulosa a la experiencia del usuario. La elección de colores, negro, naranja y blanco no solo resalta la identidad de la marca, sino que también contribuye a una interfaz amigable y atractiva. Este enfoque en el diseño no solo mejora la estética, sino que también influye en la experiencia del cliente, facilitando la navegación y fomentando la interacción.

## 2\. Integración de Tecnologías Front-end:

La implementación de Vue.js para la interactividad del lado del cliente y el uso estratégico de Bootstrap para el diseño responsivo destacan la voluntad de DELIBURGUER de mantenerse a la vanguardia de las tecnologías web. La combinación de estas tecnologías no solo garantiza un sitio web moderno, sino que también contribuye a una experiencia de usuario más dinámica y atractiva.

## 3\. Marketing Digital:

DELIBURGUER reconoce la importancia del marketing digital en la era actual. La presencia en redes sociales, como Facebook e Instagram, amplifica el alcance de la marca y permite la conexión directa con los clientes. Este enfoque estratégico refleja una comprensión profunda de las herramientas digitales para promocionar el negocio y fortalecer la relación con la clientela.

## 4\. Estrategias de Contacto y Ventas:

La inclusión de un botón de contacto directo a través de WhatsApp demuestra la voluntad de DELIBURGUER de simplificar el proceso de pedidos y establecer una comunicación directa con los clientes. Esta estrategia no solo mejora la accesibilidad para el cliente, sino que también puede contribuir a un aumento en las conversiones y la satisfacción del usuario.

## 5\. Mapa de Ubicación y Estrategias de SEO Local:

El mapa de ubicación integrado en la página no solo proporciona información esencial para los clientes locales, sino que también respalda las estrategias de SEO local. DELIBURGUER reconoce la importancia de ser fácilmente localizable en línea y cómo esto impacta positivamente en la visibilidad del negocio en la comunidad.

## 6\. Expansión a través de la Presencia en Línea:

DELIBURGUER, con su enfoque proactivo en la presencia en línea, busca expandir su alcance. La creación del sitio web no solo ofrece comodidad a los clientes para realizar pedidos desde la comodidad de sus hogares, sino que también sirve como una plataforma para llegar a nuevos clientes y comunidades.

En resumen, la hamburguesería DELIBURGUER no solo se destaca por sus exquisitas hamburguesas, sino también por su visión estratégica hacia la presencia en línea. La combinación de diseño web, tecnologías modernas, marketing digital y enfoque en la experiencia del cliente demuestra la adaptabilidad y la dedicación de DELIBURGUER a la excelencia en todos los aspectos de su operación.

El código HTML “MENU” es el más importante, como todo código HTML con Vue, tenemos las etiquetas “link”,”script”,”title”,”meta”,”head”,”!DOCTYPE” para llamar todas nuestras herramientas necesarias: los códigos CSS,nuestro Bootstrap, nuestro vue,nuestras imágenes, el “UTF-8” y el “Vieport”.

![](./README/cod1.png)

La carpeta CSS “estilo” tiene como back-ground el url de la carpeta imágenes.

![](./README/cod2.png)

Las clases carrusel ayudan para nos ayudan para el efecto de las hamburguesas y sus casillas, con el “display:flex;”:

![](./README/cod3.png)

También lo utilizamos para las imágenes y nuestros títulos:

![](./README/cod4.png)

Si nuestro botón en la pagina se escurece es gracias a este botón:

![](./README/cod5.png)






Hablando de nuestro Vue;nuestro primer baso en el return de la data es lograr que el booleano mostrar este siempre en TRUE, y dar con el objeto de “categorías”, donde obtenemos los subtítulos de nuestra página:

![](./README/cod6.png)

A continuación, para mostrar nuestros platillos, con sus descripciones, imágenes e identificadores, utilizamos demasiados “Objetos Jasón” para hacerlo posible:

![](./README/cod7.png)

![](./README/cod8.png)

menuFiltradoSegundoModal() {: Aquí se define una función llamada menuFiltradoSegundoModal.

return this.menu.filter(item => item.categ === 7);: Esta línea utiliza el método filter de los arrays en JavaScript. this.menu es un array de elementos del menú. La función filter crea un nuevo array que contiene solo los elementos que cumplen con la condición dada en la función de filtrado. En este caso, filtra los elementos cuya propiedad categ es igual a 7.

menuFiltrado() {: Aquí se define otra función llamada menuFiltrado.

const categoriasSeleccionadas = Object.keys(this.categoriaSeleccionada).filter(: Aquí se obtienen las categorías seleccionadas del objeto categoriaSeleccionada. Object.keys devuelve un array con las claves del objeto, y luego se aplica el método filter para quedarse solo con las claves cuyos valores son verdaderos. Esto implica que solo se seleccionan las categorías que están marcadas como verdaderas.

if (categoriasSeleccionadas.length === 0) {: Se verifica si no se ha seleccionado ninguna categoría. Si este es el caso:

return this.menu;: Se devuelve el menú completo sin filtrar.

return this.menu.filter(item => categoriasSeleccionadas.includes(item.categ.toString()));: Si se han seleccionado categorías:

Aquí se utiliza nuevamente filter para obtener un nuevo array que incluye solo los elementos cuya propiedad categ está presente en las categorías seleccionadas. La comparación se realiza convirtiendo item.categ a una cadena con toString() para asegurar que se comparen tipos de datos iguales.

En resumen, estos métodos permiten filtrar el menú en función de ciertos criterios, ya sea una categoría específica (en el primer método) o varias categorías seleccionadas (en el segundo método). Estos filtros son esenciales para mostrar información específica en la interfaz de usuario basada en las preferencias del usuario.

### CONCLUSION:

En resumen, hemos cumplido con los requerimientos de la empresa Deliburguer cuando una página llamativa e intuitiva para usuario. Todo es lo hemos podido lograr usando Bootstrap y las funcionalidades de vuej3.js como el v-if y v-for que han sido muy importantes para el desarrollo de la página.
 
### BIBLIOGRAFIA:

<https://faq.whatsapp.com/5913398998672934/>

<https://getbootstrap.com/docs/5.3/components/carousel/>

<https://vuejs.org/api/application.html#createapp>


