# UNIVERSIDAD PRIVADA DOMINGO SAVIO
## FACULTAD DE INGENIERIA
![](https://www.upds.edu.bo/wp-content/uploads/2020/12/upds-logo-social-preview.jpg)  
**Proyecto Final:** Tienda De Libros "Book Online"  
**Materia:** Diseño Web II  
**Docente:** Ing. JAIME ZAMBRANA CHACON  
**Integrantes:**
- Juan Ruben Heredia Padilla
- Jesus Gabriel Justiniano Enrriquez
- Jorge Gabriel Paniagua Montenegro

**Ubicación:** Santa Cruz – Bolivia

## Introducción
Me complace presentar el resultado de un proyecto que ha llevado la experiencia del curso de diseño web II a un nivel más interactivo y dinámico. Utilizando la tecnología Vue.js, HTML y CSS, hemos creado una página web que no solo es estéticamente atractiva, sino que también incorpora funcionalidades para mejorar la experiencia del usuario.

La elección de Vue.js como framework para el desarrollo de esta página web ha sido fundamental para lograr una interfaz de usuario fluida y altamente receptiva. Vue.js, con su enfoque basado en componentes y la capacidad de gestionar eficientemente el estado de la aplicación, esto ha permitido la creación de una interfaz intuitiva que se adapta a las necesidades del usuario a la hora de comprar en una tienda de libros.

La página web se centra en la exploración y búsqueda de libros, ofreciendo a los visitantes la posibilidad de filtrar y encontrar fácilmente libros de diversas categorías, como Matemáticas, Biología, Historia y Literatura.

Una de las características destacadas es la capacidad de agregar libros al carrito de compras de manera sencilla y eficiente. Cada libro seleccionado se muestra claramente en una sección de compra, junto con detalles como nombre, autor, precio y cantidad, permitiendo al usuario tener un control total sobre su selección a la hora de comprar uno o varios libros.

En resumen, esta página web desarrollada con Vue.js representa la unión entre diseño estético y funcionalidad. Al explorar esta plataforma, los usuarios experimentarán no solo una belleza visual, sino también la practicidad de una interfaz diseñada para satisfacer sus necesidades de búsqueda y compra de libros de manera eficiente y atractiva.

## Abstract
I am pleased to present the outcome of a project that has taken the experience of the Design Web II course to a more interactive and dynamic level. Using Vue.js, HTML, and CSS technologies, we have created a website that is not only aesthetically appealing but also incorporates features to enhance the user experience.

The choice of Vue.js as the framework for developing this website has been crucial in achieving a smooth and highly responsive user interface. Vue.js, with its component-based approach and efficient state management capabilities, has allowed the creation of an intuitive interface that adapts to users' needs when shopping at a bookstore.

The website focuses on the exploration and search for books, offering visitors the ability to filter and easily find books in various categories such as Mathematics, Biology, History, and Literature.

One notable feature is the ability to add books to the shopping cart easily and efficiently. Each selected book is clearly displayed in a shopping section, along with details such as name, author, price, and quantity, enabling users to have full control over their selection when purchasing one or multiple books.

In summary, this website developed with Vue.js represents the fusion of aesthetic design and functionality. Exploring this platform, users will experience not only visual beauty but also the practicality of an interface designed to meet their needs for searching and purchasing books efficiently and attractively.

## Desarrollo
### Marco Teórico
#### 1. Introducción
El desarrollo de aplicaciones web modernas implica la combinación de diversas tecnologías para crear interfaces de usuario atractivas, funcionales y receptivas. Este marco teórico analiza las tecnologías utilizadas en el proyecto, destacando su papel en la creación de una experiencia de usuario óptima.

#### 2. Tecnologías Utilizadas
##### 2.1 HTML (HyperText Markup Language)
HTML es el lenguaje estándar utilizado para estructurar el contenido de las páginas web. Define la estructura básica de una página mediante la utilización de elementos como etiquetas, atributos y elementos anidados.

##### 2.2 CSS (Cascading Style Sheets)
CSS se encarga de definir el estilo y la presentación visual de los elementos HTML. Permite personalizar la apariencia de la página web mediante la aplicación de reglas de estilo que controlan aspectos como el color, la tipografía, el diseño y la disposición de los elementos.

##### 2.3 JavaScript
JavaScript es un lenguaje de programación que se utiliza para agregar interactividad y dinamismo a las páginas web. Permite manipular el contenido HTML, responder a eventos del usuario y realizar operaciones como validación de formularios, animaciones y comunicación con el servidor.

##### 2.4 Bootstrap
Bootstrap es un framework front-end que proporciona una colección de herramientas, componentes y estilos predefinidos para el desarrollo rápido y eficiente de interfaces de usuario. Si bien es una herramienta útil, no es el único enfoque para el diseño web y se puede combinar con otras tecnologías para lograr resultados personalizados.

#### 3. Integración de Tecnologías
##### 3.1 HTML, CSS y JavaScript
La integración de HTML, CSS y JavaScript permite crear páginas web interactivas y dinámicas. HTML define la estructura del contenido, CSS controla el estilo y la presentación visual, mientras que JavaScript añade funcionalidades interactivas y comportamientos dinámicos a la página.

##### 3.2 Frameworks y Bibliotecas
Además de Bootstrap, existen otros frameworks y bibliotecas front-end disponibles, como Foundation, Materialize CSS, y Tailwind CSS, que ofrecen alternativas para el diseño y desarrollo de interfaces de usuario. Estas herramientas proporcionan estilos predefinidos, componentes personalizables y funcionalidades adicionales que pueden adaptarse a las necesidades del proyecto.

#### 4. Ventajas de la Integración
##### 4.1 Flexibilidad y Personalización
La integración de múltiples tecnologías permite una mayor flexibilidad y personalización en el diseño y desarrollo de la interfaz de usuario. Los desarrolladores pueden aprovechar las características únicas de cada tecnología y combinarlas de manera creativa para crear experiencias de usuario únicas y atractivas.

##### 4.2 Optimización de Rendimiento
Al utilizar técnicas de optimización de código y prácticas recomendadas, es posible mejorar el rendimiento de la página web, reduciendo los tiempos de carga y proporcionando una experiencia de usuario más fluida y receptiva.

##### 4.3 Mejora de la Usabilidad
La integración de tecnologías bien diseñadas y probadas puede contribuir a mejorar la usabilidad de la página web, facilitando la navegación, la interacción y la comprensión del contenido por parte de los usuarios.

### Proyecto
#### Uso del json
Entre las funciones implementadas para el funcionamiento de la página web, podemos destacar el uso de Vue.js con JSON, la cual llama a los datos guardados en un array de los libros que la tienda de libros tiene en su inventario.

**Código 1:**
```javascript
Vue.createApp({
    data() {
        return {
            mostrar: false,
            filtro: '',
            filtros: [],
            libro: [
                { id:"1",nombre: "El Codigo Da Vinci", materia: "solicitado", autor: "Dan Brown", foto: "img/El-codigo-da-vinci.webp", reseña:"Una intrigante novela de misterio y suspense que sigue al profesor Robert Langdon en su búsqueda de revelaciones ocultas en obras de arte y símbolos, desentrañando secretos que podrían cambiar la historia tal como la conocemos.", precio: 30,cantidad:1,stock:90 },
                { id:"2",nombre: "El Señor de los Anillos", materia: "solicitado", autor: "J. R. R. Tolkien", foto: "img/El-Senor-de-los-Anillos-Ilustrado-por-Alan-Lee.webp", reseña:"Una épica fantasía que sigue la odisea de Frodo Bolsón para destruir un anillo maligno y salvar a la Tierra Media. Llena de personajes inolvidables y un mundo ricamente detallado, es una obra maestra del género.", precio: 30,cantidad:1,stock:80 },
                // ... Otros libros
            ]
        };
    },
    methods: {
        // Métodos de Vue.js
    }
});
```
**Código 1.1:**
```javascript
<div v-if="mostrarLiteratura">
                            <h1>Libros de Literatura</h1>
                            <span v-for="book in libro" :key="book.id">
                                <div v-if="book.materia == 'literatura'"class="book-container portada-book">
                                    <div>
                                        <img :src="book.foto" alt="Portada del libro">
                                    </div>
                                    <div class="book-info">
                                        <p class="book-title">Titulo : {{ book.nombre }}</p>
                                        <p class="book-author">Autor : {{ book.autor }}</p>
                                        <p class="book-reseña">Descripcion : {{book.reseña}}</p>
                                    </div>
                                    Cantidad a comprar: <input type="number" v-model="book.cantidad"  min="0" :max="book.stock">
                                    <p>Precio Unitario: Bs {{book.precio}}</p>
                                    <p>Cantidad de libros: {{book.stock}} Unid.</p>
                                    <div class="btn-2">
                                        <button @click="Carrito(book)" class="book-button">Añadir al</button>
                                        <button class="fa-solid fa-cart-shopping mas-detalle"></button>
                                    </div>
                                </div>
```
### Barra lateral izquierda para Filtrar libros por categoria
La pagina también cuenta con una barra izquierda donde podemos seleccionar los libros que el usuario quiere para que pueda seleccionar el genero que mas le guste, desde matemática, historia, biología y literatura, este esta de la siguiente manera:
**Código 2: Estructura de la Barra Lateral**
En el código HTML y Vue.js, se define la estructura de la barra lateral. Se proporcionan opciones de filtro mediante casillas de verificación para los géneros de libros, como Matemáticas, Literatura, Biología e Historia. Estas opciones son controladas por las variables de Vue.js, como mostrarMatematicas, mostrarLiteratura, mostrarBiologia y mostrarHistoria.
```javascript
<div class="pantalla-dividida">
            <div class="menu-lateral">
                <h5>Buscar Libros</h5>
                <label>Seleccione el libro a buscar</label>
                <ul>
                    <li><input type="checkbox" v-model="mostrarMatematicas">Matematicas</li>
                    <li><input type="checkbox" v-model="mostrarLiteratura">Literatura</li>
                    <li><input type="checkbox" v-model="mostrarBiologia">Biologia</li>
                    <li><input type="checkbox" v-model="mostrarHistoria">Historia</li>
                </ul>
            </div>

```
**Código 2.1: Manejo de Estados y Variables Vue.js**
En este bloque de código Vue.js, se definen las variables que controlan el estado de la barra lateral. Por ejemplo, mostrarMatematicas se utiliza para determinar si los libros de Matemáticas deben mostrarse. Además, otras variables como mostrar, masDetalle, y cantidadCarrito se utilizan para gestionar otros aspectos de la página.
```javascript
Vue.createApp({
    data() {
        return {
            mostrar: false,
            masDetalle:false,
            mostrarMatematicas: false,
            mostrarBiologia: false,
            mostrarHistoria: false,
            mostrarLiteratura: false,
            cantidadCarrito: 0,
            mostrarCarrito:false,
            deshabilitar:true,
            habilitar:true,
            cantidadExcedida:'Ingrese una cantidad inferior a la cantidad de libros',
            cantidadInferior:'Ingrese una cantidad superior a 0',
            filtro: '',
            filtros: [],
```
**Código 2.2: Métodos Vue.js para la Búsqueda y Restablecimiento**
Se proporcionan métodos en Vue.js para realizar la búsqueda de libros en función de los géneros seleccionados. La función BuscarLibros() muestra los libros filtrados en la página y utiliza una alerta para visualizar los resultados.
La función mostrarInicio() restablece el estado de la página a su configuración inicial, ocultando los resultados de la búsqueda y desmarcando las opciones de género seleccionadas.
```javascript
methods:{
        BuscarLibros() {
            this.mostrar = true;
            alert(this.libro.filter(libro => {libro.name.toLowerCase() === this.filtro.toLowerCase()}));
        },
        mostrarInicio(){
            this.mostrar = false;
            this.filtro= '';
            this.books= [];
            this.mostrarMatematicas= false;
            this.mostrarBiologia= false;
            this.mostrarHistoria= false;
            this.mostrarLiteratura= false;
        },
```
### Implementación del carrito de compras 
Este método lo que hace es que cuando un usuario quiere comprar un libro este se almacena en el carrito de compras donde se mostrar el libro comprado desde matemática, literatura, biología o historia con su precio y la cantidad que desea comprar el usuario o cliente el código es el siguiente:
Código 3: Interfaz del Carrito
El código HTML y Vue.js asociado se encarga de la interfaz del carrito. La variable mostrarCarrito controla la visibilidad de la ventana emergente. Cuando el usuario agrega libros al carrito, se actualiza dinámicamente la cantidad total de libros (cantidadCarrito), y se muestran los detalles de cada libro, incluyendo su ID, nombre, cantidad seleccionada, precio unitario y el total por libro.
```javascript
<div class="btn-2">
                <a href="formulario.html" class="btn nav-btn"><button class="fa-solid fa-user"></button></a>
                <a @click="openCarr" class="btn nav-btn menuCarrito">
                    <button class="fa-solid fa-cart-shopping">
                        <span class="notificacion">{{cantidadCarrito}}</span>
                    </button>
                </a>
                <a href="formulario.html" class="btn nav-btn"><button>contactos</button></a>
            </div>
            <div v-if="mostrarCarrito" class="overlayCarrito">
                <a @click="closeCarr" class="close-carrito"><i class="fa-solid fa-arrow-left">Salir</i></a>
                <span v-if="cantidadCarrito == 0">
                    <p>No Tiene Agregado ningun Libro en el Carrito</p>
                </span>
                <span v-if="cantidadCarrito >= 1">
                    <table>
                        <thead>
                            <tr>
                              <th >Id</th>
                              <th >Nombre del Libro</th>
                              <th >Cantidad</th>
                              <th >precio</th>
                              <th >total</th>
                            </tr>
                          </thead>
                          <tbody>        
                            <tr v-for="product in libros">
                              <th>{{product.id}}</th>
                              <td>{{product.nombre}}</td>
                              <td>{{product.cantidad}}</td>
                              <td>Bs.{{product.precio}}</td>
                              <td>Bs.{{product.cantidad*product.precio}}</td>
                              <td class="btn-2">
                                <button @click="Comprar(product)" class="book-button">Comprar</button>
                              </th>
                            </tr>       
                          </tbody>
                    </table>
                </span>
```
**Código 3.1: Funciones para Abrir y Cerrar el Carrito**
Las funciones openCarr y closeCarr manejan la apertura y cierre del carrito, respectivamente. Estas funciones ajustan la posición y tamaño de la ventana emergente del carrito para proporcionar una experiencia visual agradable al usuario.
```javascript
function openCarr(){
    document.getElementById("carrito-menu").style.top = "20%";
    document.getElementById("carrito-menu").style.left = "60%";
    document.getElementById("carrito-menu").style.width = "38%";
}
function closeCarr(){
    document.getElementById("carrito-menu").style.display="none";
}
```
**Código 3.2: Gestión del Carrito y Confirmación de Compra**
La función Carrito(producto) se encarga de añadir libros al carrito, asegurándose de que la cantidad seleccionada sea válida y no exceda el stock disponible. Además, se proporcionan mensajes informativos si la cantidad es inválida.
La función Comprar(producto) verifica nuevamente la cantidad antes de confirmar la compra, mostrando mensajes de éxito o mensajes de error según corresponda.
```javascript
Carrito(producto){
            if ((producto.cantidad > 0)&&(producto.cantidad<=producto.stock)) {
                this.libros.push(producto);
                this.cantidadCarrito++;
            }else{
                if (producto.cantidad <=0) {
                    this.cantidadInferior;
                }else{
                    this.cantidadExcedida;
                }
            }
        },
        Comprar(producto){
            if ((producto.cantidad > 0)&&(producto.cantidad<=producto.stock)) {
                alert("su compra se realizo con exito")
            }else{
                if (producto.cantidad <=0) {
                    alert("no se puede realizar la compra introduzca un numero valido")
                }else{
                    alert("no se puede realizar la compra ya que la cantidad de libros seleccionada supera el limite de libros")
                }
            }
        },
        openCarr(){
            this.mostrarCarrito=true;
        },
        closeCarr(){
            this.mostrarCarrito=false;
        }
```
### Codigo Fuente(Link)	
https://drive.google.com/drive/folders/1l8zKNuqWVqknJ_suVzHVD1lkQG3Jl9ix?usp=sharing

## 5. Conclusión

La combinación de HTML, CSS, JavaScript y frameworks front-end como Bootstrap ofrece un enfoque integral para el diseño y desarrollo de interfaces de usuario efectivas. Al integrar estas tecnologías de manera adecuada y aprovechar sus capacidades únicas, es posible crear experiencias de usuario atractivas y funcionales que cumplan con los objetivos del proyecto.
La elección de Vue.js como el marco principal para el desarrollo de la interfaz de usuario ha demostrado ser fundamental para lograr una plataforma receptiva y dinámica. Esta tecnología, junto con HTML y CSS, ha permitido la creación de una interfaz atractiva que se adapta a las necesidades del usuario al explorar y comprar libros en línea.
La página web se destaca por su enfoque intuitivo en la búsqueda y filtrado de libros, proporcionando a los usuarios una experiencia de navegación fluida y personalizada. La inclusión de categorías como Matemáticas, Biología, Historia y Literatura facilita la ubicación de libros específicos, mejorando la eficiencia del proceso de selección.  
![](https://th.bing.com/th/id/R.3b0e0d39edfc8455d857845708bae707?rik=nIcLGzKAE6mqCA&riu=http%3a%2f%2f4.bp.blogspot.com%2f-akzsGwj-scg%2fVoH7LKBhJ5I%2fAAAAAAAABA4%2fJrtLZO1PXQ0%2fs1600%2fimagenes-gif-de-libros-1.gif&ehk=4LJHREK0C51wwf0cR3p2t6w2tLCDWwUjwa8AO1wMTf0%3d&risl=&pid=ImgRaw&r=0)

 
## Bibliografía

#### Introduction | Vue.js (vuejs.org)
#### CSS Tutorial (w3schools.com)
#### HTML Tutorial (w3schools.com)



