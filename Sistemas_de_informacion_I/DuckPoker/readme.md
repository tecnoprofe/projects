#  Nombre del equipo: DuckQuartic

#  Proyecto QueryQuest

El proyecto **QueryQuest** tiene como objetivo crear un sistema completo que abarca tanto el **Backend** como el **Frontend**, con el propósito específico de administrar una API que se utilizará en un videojuego de preguntas desarrollado en Unity2D. Esta API contendrá diversas categorías, como deporte, historia, matemáticas, entretenimiento y más.

## Bocetos

### Idea del Proyecto
Aquí está una vista preliminar de la idea central del proyecto:

![Bocetos de la idea](Idea.png)

### Interfaz de frontend
Ideas visual de como se vera la plataforma:

![Bocetos de la Interfaz de Administración](FrontEnd.png)

### Diagrama de la base de datos
Diagrama de la base de datos:

![Bocetos del diagrama de la base de datos](DiagramaBD.png)

## Integrantes

- Mauricio Alexander Flores Morales
- Jose Yosmark Saavedra Vaca
- Abrahan Rueda Mendez
- Rodrigo Lozano Guzman

## Tecnologías a Utilizar

- ASP.NET Core
- PHP
- Microsoft SQL Server
- Unity
- C#

## Distribución de Tareas

### Frontend:
- **Mauricio Alexander Flores Morales**
- **Jose Yosmark Saavedra Vaca**

Estaran encargados de la parte visual que administra la api en la web.

### Backend:
- **Abrahan Rueda Mendez**
- **Rodrigo Lozano Guzman**

Se encargaran de toda la logica que permite guardar o modificar registros en la base de datos.

### Unity:
- **Abrahan Rueda Mendez**
  - Código
- **Rodrigo Lozano Guzman**
  - Imágenes, visuales, Código
- **Mauricio Alexander Flores Morales**
  - Letras, para la interfaz, Código
- **Jose Yosmark Saavedra Vaca**
  - Sonido, para que el juego sea más intuitivo, Código


